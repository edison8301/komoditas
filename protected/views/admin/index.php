<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<h1>Dashboard Pergerakan Harga 30 Hari Terakhir</h1>


<?php 
    $komoditas = null;
    $lokasi = null;
    
   
    
?>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
  'id'=>'bahan-export-form',
  'type'=>'horizontal',
  'enableAjaxValidation'=>false,
)); ?>

<div class="well">
    <?php echo $form->select2Group($model,'komoditas',array(
        'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
            'widgetOptions'=>array(
                'htmlOptions'=>array('empty'=>'-- Pilih Komoditas --'),
                'data'=>Komoditas::getListKomoditas()
            )
    )); ?>

     <?php echo $form->select2Group($model,'lokasi',array(
         'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
            'widgetOptions'=>array(
                'htmlOptions'=>array('empty'=>'-- Pilih Lokasi --'),
                'data'=>CHtml::listData(Lokasi::model()->findAll(),'id','nama')
            ),
            'prepend' => '<i class="glyphicon glyphicon-map-marker"></i>'
    )); ?>
</div>

<div class="form-actions well">
  <div class="row">
    <div class="col-sm-3">&nbsp;</div>
    <div class="col-sm-9">
    <?php $this->widget('booster.widgets.TbButton',array(
          'label' => 'Tampilkan Grafik Pergerakan Harga',
          'buttonType' => 'submit',
          'context' => 'success',
          'icon'=>'signal'
    )); ?>
    </div>
  </div>
</div>


<?php $this->endWidget(); ?>

<?php $this->renderPartial('_chart',array('lokasi'=>$model->lokasi,'komoditas'=>$model->komoditas)); ?>
