<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'bahan-pokok-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="well">
	<?php echo $form->dropDownListGroup($model,'id_induk',array('widgetOptions'=>array('htmlOptions'=>array('empty'=>'-- Pilih Parent --'),'data'=>CHtml::listData(Bahan::model()->findAllByAttributes(array('id_induk'=>null)),'id','nama')))); ?>
	
	<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->textFieldGroup($model,'satuan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->textFieldGroup($model,'urutan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	</div>

	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Simpan',
			'icon'=>'ok'
		)); ?>
	</div>

<?php $this->endWidget(); ?>
