<?php foreach($model as $data) { ?>
<tr>
	<td>&nbsp;</td>
	<td>
		<?php print $data->nama; ?>
		<?php print CHtml::link("<i class='glyphicon glyphicon-pencil'></i>",array('bahan/update','id'=>$data->id,'id_bahan'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Update Bahan')); ?>
		
	</td>
	<td style="text-align:center"><?php print $data->satuan; ?></td>
	<?php foreach(Lokasi::model()->findAll() as $lokasi) { ?>
	<td style="text-align:right"><?php print number_format(Harga::model()->getHarga($data->id,$lokasi->id,date('Y-m-d')),0,',','.'); ?>
		<?php print CHtml::link("<i class='glyphicon glyphicon-pencil'></i>",array('harga/create','id_lokasi'=>$lokasi->id,'id_bahan'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>
	</td>
	<?php } ?>
	<?php if($data->hasSub()) { ?>
	<td>&nbsp;</td>
	<?php } else { ?>
	<td style="text-align:right"><?php print number_format(Harga::model()->getHargaRata($data->id,date('Y-m-d')),0,',','.'); ?></td>
	<?php } ?>
	<td style="text-align:center">	
		<?php print CHtml::link("<i class='glyphicon glyphicon-arrow-up'></i>",array('bahan/naikUrutan','id'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Naik Urutan')); ?>
		<?php print CHtml::link("<i class='glyphicon glyphicon-arrow-down'></i>",array('bahan/turunUrutan','id'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Turun Urutan')); ?>
	</td>
</tr>
<?php } ?>