
<div id="chartContainer" class="text-center"></div>

<!-- PANGGIL EXTENSIONS JS CHART-->
<script type="text/javascript" 
  src="<?php echo Yii::app()->request->baseUrl; ?>/vendors/fusioncharts/js/fusioncharts.js">
</script>
<script type="text/javascript" 
  src="<?php echo Yii::app()->request->baseUrl; ?>/vendors/fusioncharts/js/themes/fusioncharts.theme.fint.js">
</script>


<!-- SCRIPT FUSION CHART -->
<script>
  FusionCharts.ready(function(){
  var revenueChart = new FusionCharts({
    "type": "line",
    "renderAt": "chartContainer",
    "width": "100%",
    "height": "400",
    "dataFormat": "json",
    "dataSource": {
      "chart": {
        "caption": "Pergerakan Harga <?= Komoditas::getNamaKomoditasStatis($komoditas); ?>" ,
        "subCaption": "Lokasi : <?= Lokasi::getLokasiStatis($lokasi); ?>",
        "xAxisName": "Tanggal",
        "yAxisName": "Harga",
        "paletteColors": "#0075c2",
        "baseFontColor": "#333333",
        "baseFont": "Helvetica Neue,Arial",
        "captionFontSize": "20",
        "formatNumberScale": false,
        "subcaptionFontSize": "16",
        "subcaptionFontBold": "0",
        "showBorder": "1",
        "bgColor": "#efefef",
        "showShadow": "1",
        "canvasBgColor": "#ffffff",
        "canvasBorderAlpha": "0",
        "divlineAlpha": "100",
        "divlineColor": "#666",
        "divlineThickness": "1",
        "showValues":false,
        "divLineDashed": "1",
        "canvasPadding": "0",
        "captionPadding": "20",
        "divLineDashLen": "3",
        "divLineGapLen": "1",
        "showXAxisLine": "1",
        "xAxisLineThickness": "1",
        "yAxisLineThickness": "1",
        "xAxisLineColor": "#999999",
        "yAxisLineColor": "#999999",
        "showAlternateHGridColor": "0",
      },
      "data": [ <?php print Harga::getGrafik($lokasi,$komoditas); ?> ]
    }
  });
    revenueChart.render();
  })
</script>   
