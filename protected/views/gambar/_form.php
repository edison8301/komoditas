<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'gambar-form',
	'type' => 'horizontal',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<div class="well">

		<?php echo $form->textFieldGroup($model,'nama',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)
		); ?>

		<?php echo $form->fileFieldGroup($model,'gambar',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-3'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5',
						'maxlength'=>255
					)
				)
			)
		); ?>

		<?php echo $form->select2Group($model,'id_kategori',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-3'),
				'widgetOptions'=>array(
					'data' => GambarKategori::getList(),
					'htmlOptions'=>array(
						'class'=>'span5'
					)
				)
			)
		); ?>


	</div>

	<div class="form-actions well">
		<div class="row">
			<div class="col-sm-3">&nbsp;</div>
			<div class="col-sm-9">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'success',
					'icon'=>'ok',
					'label'=>'Simpan',
				)); ?>

				
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'link',
					'context'=>'danger',
					'icon'=>'remove',
					'label'=>'Batal',
					'url'=>Yii::app()->request->Urlreferrer
				)); ?>
			</div>
		</div>
	</div>


<?php $this->endWidget(); ?>
