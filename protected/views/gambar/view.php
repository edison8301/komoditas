<?php
$this->breadcrumbs=array(
	'Gambars'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Gambar','url'=>array('index')),
array('label'=>'Create Gambar','url'=>array('create')),
array('label'=>'Update Gambar','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Gambar','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Gambar','url'=>array('admin')),
);
?>

<h1><?php echo $model->nama; ?></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Ubah Gambar',
		'icon'=>'pencil',
		'context'=>'success',
		'url'=>array('update','id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered condensed',
'attributes'=>array(
		'nama',
		[
			'name' => 'id_kategori',
			'value' => $model->getRelation("kategori","nama")
		],
		[
			'name' => 'gambar',
			'value' => $model->getGambar(['class' => 'img-responsive']),
			'type' => 'raw'

		],
),
)); ?>
