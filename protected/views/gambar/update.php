<?php
$this->breadcrumbs=array(
	'Gambars'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Gambar','url'=>array('index')),
	array('label'=>'Create Gambar','url'=>array('create')),
	array('label'=>'View Gambar','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Gambar','url'=>array('admin')),
	);
	?>

	<h1>Sunting <?php echo $model->nama; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>