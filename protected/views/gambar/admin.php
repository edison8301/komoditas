<?php
$this->breadcrumbs=array(
	'Gambars'=>array('index'),
	'Manage',

);
?>

<h1>Gambar</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Gambar',
		'icon'=>'plus',
		'context'=>'success',
		'url'=>array('gambar/create','id_kategori' => $id_kategori)
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'gambar-grid',
		'dataProvider'=>$model->search($id_kategori),
		'filter'=>$model,
		'type' => 'striped bordered condensed',
		'columns'=>array(
                array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                    'headerHtmlOptions'=>array('style'=>'width:5%;text-align:center'),
                    'htmlOptions'=>array('style'=>'width:5%;text-align:center'),
                ),
				'nama',
				[
					'name' => 'id_kategori',
					'value' => '$data->getRelation("kategori","nama")'
				],
				array(			
					'header'=>'Gambar',
					'name'=>'gambar',
					'type'=>'raw',
					'value'=>'$data->getGambar(array("class"=>"img-responsive"))',
					'headerHtmlOptions'=>array('style'=>'width:50%'),
		            'htmlOptions'=>array('style'=>'text-align:center;width:50%'),
					'filter'=>''	
				),
		array(
		'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>

<div>&nbsp;</div>
