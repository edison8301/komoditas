<?php
$this->breadcrumbs=array(
	'Gambars'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Gambar','url'=>array('index')),
array('label'=>'Manage Gambar','url'=>array('admin')),
);
?>

<h1>Create Gambar</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>