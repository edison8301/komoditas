<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin/main'); ?>

<div class="row" id="leftMenu" >
	<div class="col-sm-2" >
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'htmlOptions' => array('class' => 'left-menu-style'),
			'items' => array(
				array('label' => 'Dashboard','icon'=>'home', 'url' => array('admin/index')),
				array('label'=>'Gambar','icon'=>'picture','items'=>array(
					array('label'=>'Informasi Tunggakan','icon'=>'wrench','url'=>array('/gambar/update','id'=>1)),
					array('label'=>'Foto','icon'=>'user','url'=>array('/gambar/admin','id_kategori' => 2)),
				)),
				array('label' => 'Komoditas','icon'=>'usd', 'url' => array('komoditas/index')),
				array('label' => 'Lokasi','icon'=>'map-marker', 'url' => array('lokasi/admin')),
				array('label' => 'Informasi','icon'=>'bullhorn', 'url' => array('informasi/admin')),
				array('label' => 'User','icon'=>'user', 'url' => array('user/admin')),
				array('label' => 'Export Data','icon'=>'export', 'url' => array('komoditas/export')),
				array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
			)
	)); ?>
	</div>


	<div class="col-sm-10">
		<div class="row">
			<div class="col-lg-12">
				<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
					<div class="alert alert-<?php print $key; ?>"><?php print $message; ?></div>
				<?php } ?>
				<?php Yii::app()->clientScript->registerScript('hideAlert',
						'$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
						CClientScript::POS_READY
				); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<?php echo $content; ?>
			</div>
		</div>
	</div><!-- content -->
</div>



<?php $this->endContent(); ?>
