<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css" />
	<script src="<?php print Yii::app()->request->baseUrl; ?>/vendors/accounting/accounting.min.js" type="text/javascript"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<script>

$(document).ready(function(){
	
	$(".format-money").on("keyup", function(){
	    var _this = $(this);
	    var value = _this.val().replace(/\.| /g,"");
	    _this.val(accounting.formatMoney(value, "", 0, ".", ","))
	});

	$(window).scroll(function() {
  		$("body").scroll("slow");
	});

});

</script>


<body>

<div class="header">
	<img width="800px" class="logo" src="<?php print Yii::app()->baseUrl; ?>/images/logo.png">
</div>


<div id="adminnav">
<?php $this->widget('booster.widgets.TbNavbar',array(
			'brand' => '',
			'fixed' => false,
			'fluid' => true,
			'type'=>'',
			'items' => array(
				array(
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'items' => array(
						array('label' => 'Dashboard','icon'=>'home', 'url' => array('admin/index')),
						array('label' => 'Lihat Display','icon'=>'search', 'url' => array('site/index'),'linkOptions'=>array('target'=>'_blank')),
						array('label' => 'Ganti Password','icon'=>'lock', 'url' => array('user/gantiPassword')),
						array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
					)
				)
			)
)); ?>
</div>
	
<div class="containers" id="page">

	<div class="row">
		<div class="col-lg-12">
			<?php echo $content; ?>
		</div>
	</div>
</div>

<div id="footer" style="margin-top:150px;">
	Copyright &copy; 2016 by Dinas Perindustrian Perdagangan dan Koperasi Kabupaten Belitung Timur
</div><!-- footer -->

</div><!-- page -->

</body>
</html>
