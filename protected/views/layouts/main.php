<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

  

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

  <script>

  $(document).ready(function(){
    
    $(".format-money").on("keyup", function(){
        var _this = $(this);
        var value = _this.val().replace(/\.| /g,"");
        _this.val(accounting.formatMoney(value, "", 0, ".", ","))
    });

    $(window).scroll(function() {
        $("body").scroll("slow");
    });

  });

</script>

<body id="frontend">

<div id="page">
	<?php echo $content; ?>
</div><!-- page -->

<div id="footer">
  	<div id="jam">
    	<span id="datetime"></span>
    	<script type="text/javascript"> window.onload = date_time('datetime'); </script>
   		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/datetime.js"); ?>
  	</div>
  	<div id="informasi">
        <marquee behavior="scroll" direction="left" align="center" scrollamount="10" >    
          <?php foreach(Informasi::model()->findAll(array('order'=>'urutan ASC')) as $informasi) { ?>
            <span class="informasi">&nbsp;{--------}&nbsp; <?= $informasi->nama_informasi; ?></span>
          <?php } ?>
        </marquee>
    </div>
</div>
</body>


</html>
