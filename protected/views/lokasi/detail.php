<?php
    
    $hariIni = date('Y-m-d');
    $kemarin = date( 'Y-m-d',strtotime('-1 days'));
    $kemarinLusa = date( 'Y-m-d',strtotime('-2 days'));

?>

<div class="container" style="background-color: #fff;padding: 25px;">
<div class="row">
    <div class="col-sm-12">
        <h1 class="text-center">Harga Komoditas di <b><?php print $model->nama ?></b></h1>
        <h3 class="text-center">Per <?php print Helper::getTanggalSingkat($kemarin); ?> - 
            <?php print Helper::getTanggalSingkat($hariIni); ?>        
        </h3>
        <div>&nbsp;</div>
        
        <table class="table table-bordered table-condensed table-hover" style="table-layout: fixed;">    
            <tr>
                <th rowspan="2" style="text-align: center" width="40%">Komoditas</th>
                <th colspan="2" style="text-align: center" width="30%">Harga</th>
                <th colspan="2" style="text-align: center" width="30%">Stok</th>        
            </tr>
            <tr>        
                <th style="text-align:center"><?php print Helper::getTanggalSingkat($kemarin); ?></th> 
                <th style="text-align:center"><?php print Helper::getTanggalSingkat($hariIni); ?></th>                
                <th style="text-align:center"><?php print Helper::getTanggalSingkat($kemarin); ?></th> 
                <th style="text-align:center"><?php print Helper::getTanggalSingkat($hariIni); ?></th>                
            </tr>        
        <?php $i=1; foreach(Komoditas::findAllKomoditasInduk() as $data) { ?>
           <tr>
                <?php if(!$data->hasSub()) { ?>
                    <td><?php print $data->nama; ?></td>
                    <td style="width:20%;text-align:right">
                        <?php print Helper::rp(Harga::getHarga($data->id,$model->id,$kemarin)); ?>                    
                    </td>
                    <td style="width:20%;text-align:right">
                        <?php print Helper::rp(Harga::getHarga($data->id,$model->id,$hariIni)); ?>                    
                    </td>
                    <td style='text-align:right'>
                        <?php print Helper::rp(Stok::getStok($data->id,$model->id,$kemarin)); ?>                    
                    </td>
                    <td style='text-align:right'>
                        <?php print Helper::rp(Stok::getStok($data->id,$model->id,$hariIni)); ?>                    
                    </td>

                <?php } ?>    

                <?php if($data->hasSub()) { ?>
                    <?php $this->renderPartial('_detail',array(
                            'data'=>$data,
                            'model'=>$model,
                            'hariIni'=>$hariIni,
                            'kemarin'=>$kemarin,
                            'kemarinLusa'=>$kemarinLusa
                    )); ?>
                <?php } ?>              
            </tr>
        <?php } ?>
        </table>
        

    </div>

</div>
</div>
