<?php foreach($data->findAllSub() as $data) { ?>
<tr>
    <td><?php print $data->nama; ?>&nbsp;(<?php print $data->satuan; ?>)</td>    


    <?php if (Harga::getHarga($data->id,$model->id,$kemarinLusa) == Harga::getHarga($data->id,$model->id,$kemarin)) { ?>    
        <td style="text-align:right">
               <?php print Helper::rp(Harga::getHarga($data->id,$model->id,$kemarinLusa)); ?>        
               <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('harga/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$kemarin),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>
        </td>
    <?php } else { ?>            
        <td style="text-align:right">
            <?php 
                $persen = Harga::getPersentaseHarga($data->id,$model->id,$kemarin);
                if ($persen < 0) {
                    print CHtml::image(Yii::app()->request->baseUrl.'/images/down.png').'&nbsp;';
                    print '('.Harga::getPersentaseHarga($data->id,$model->id,$kemarin).'%)';
                } else {
                    print CHtml::image(Yii::app()->request->baseUrl.'/images/up.png').'&nbsp;';
                    print '('.Harga::getPersentaseHarga($data->id,$model->id,$kemarin).'%)';
                }
            ?>                
            <?php print Helper::rp(Harga::getHarga($data->id,$model->id,$kemarin)); ?>            
            <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('harga/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$kemarin),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>
        </td>
    <?php } ?>

    <?php if (Stok::getStok($data->id,$model->id,$kemarinLusa) == Stok::getStok($data->id,$model->id,$kemarin)) { ?>    
        <td style="text-align:right">                
               <?php print Helper::rp(Stok::getStok($data->id,$model->id,$kemarin)); ?>        
               <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('stok/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$kemarin),array('data-toggle'=>'tooltip','title'=>'Input Stok Baru')); ?>
        </td>

    <?php } else { ?>            
        <td style="text-align:right">
            <?php 
                $persen = Stok::getPersentaseStok($data->id,$model->id,$kemarin);
                if ($persen < 0) {
                    print CHtml::image(Yii::app()->request->baseUrl.'/images/down.png').'&nbsp;';
                    print '('.Stok::getPersentaseStok($data->id,$model->id,$kemarin).'%)';
                } else {
                    print CHtml::image(Yii::app()->request->baseUrl.'/images/up.png').'&nbsp;';
                    print '('.Stok::getPersentaseStok($data->id,$model->id,$kemarin).'%)';
                }
            ?>                
            <?php print Helper::rp(Stok::getStok($data->id,$model->id,$kemarin)); ?>            
            <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('stok/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$kemarin),array('data-toggle'=>'tooltip','title'=>'Input Stok Baru')); ?>
        </td>
    <?php } ?>

    <?php if (Harga::getHarga($data->id,$model->id,$kemarin) == Harga::getHarga($data->id,$model->id,$hariIni)) { ?>    
        <td style="text-align:right">                
               <?php print Helper::rp(Harga::getHarga($data->id,$model->id,$kemarin)); ?>        
               <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('harga/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$hariIni),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>
        </td>

    <?php } else { ?>            
        <td style="text-align:right">
            <?php 
                $persen = Harga::getPersentaseHarga($data->id,$model->id,$kemarin);
                if ($persen < 0) {
                    print CHtml::image(Yii::app()->request->baseUrl.'/images/down.png').'&nbsp;';
                    print '('.Harga::getPersentaseHarga($data->id,$model->id,$kemarin).'%)';
                } else {
                    print CHtml::image(Yii::app()->request->baseUrl.'/images/up.png').'&nbsp;';
                    print '('.Harga::getPersentaseHarga($data->id,$model->id,$kemarin).'%)';
                }
            ?>                
            <?php print Helper::rp(Harga::getHarga($data->id,$model->id,$kemarin)); ?>            
            <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('harga/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$hariIni),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>
        </td>
    <?php } ?>

    <?php if (Stok::getStok($data->id,$model->id,$kemarin) == Stok::getStok($data->id,$model->id,$hariIni)) { ?>    
        <td style="text-align:right">
               <?php print Helper::rp(Stok::getStok($data->id,$model->id,$kemarin)); ?>        
               <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('stok/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$hariIni),array('data-toggle'=>'tooltip','title'=>'Input Stok Baru')); ?>
        </td>
    <?php } else { ?>            
        <td style="text-align:right">
            <?php 
                $persen = Stok::getPersentaseStok($data->id,$model->id,$hariIni);
                if ($persen < 0) {
                    print CHtml::image(Yii::app()->request->baseUrl.'/images/down.png').'&nbsp;';
                    print '('.Stok::getPersentaseStok($data->id,$model->id,$hariIni).'%)';
                } else {
                    print CHtml::image(Yii::app()->request->baseUrl.'/images/up.png').'&nbsp;';
                    print '('.Stok::getPersentaseStok($data->id,$model->id,$hariIni).'%)';
                }
            ?>                
            <?php print Helper::rp(Stok::getStok($data->id,$model->id,$hariIni)); ?>            
            <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('stok/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$hariIni),array('data-toggle'=>'tooltip','title'=>'Input Stok Baru')); ?>
        </td>
    <?php } ?>

    


</tr>
<?php } ?>
