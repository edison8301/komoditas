<?php
$this->breadcrumbs=array(
	'Lokasi'=>array('admin'),
	'Kelola'	
);
?>

<?php
  
    $hariIni = date('Y-m-d');
    $kemarin = date( 'Y-m-d',strtotime('-1 days'));
    $kemarinLusa = date( 'Y-m-d',strtotime('-2 days'));

?>

<h1><?php print $model->nama ?></h1>

<div>&nbsp;</div>


<div class="well" style="text-align: right">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('lokasi/update','id'=>$model->id),
		'label'=>'Sunting',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'pencil'
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('lokasi/create'),
		'label'=>'Tambah',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'plus'
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('lokasi/detail','id'=>$model->id),
		'label'=>'Lihat Lokasi',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'search',
		'htmlOptions'=>array('target'=>'_blank')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('lokasi/admin'),
		'label'=>'Kelola',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'list'
)); ?>&nbsp;
</div>

<table class="table table-condensed table-hover" style="table-layout: fixed;">
	<thead>
		<tr>			
			<th rowspan="3" style="text-align: center;vertical-align:middle" width="30%">Komoditas</th>
			<th colspan="4" style="text-align: center">Perbandingan Harga Hari Ini</th>
		</tr>		
		<tr>
			<th colspan="2" style="text-align: center"><?php print Helper::getTanggalSingkat($kemarin); ?></th>
			<th colspan="2" style="text-align: center"><?php print Helper::getTanggalSingkat($hariIni); ?></th>
		</tr>
		<tr>
			<th style="text-align: center">Harga</th>
			<th style="text-align: center">Stok</th>
			<th style="text-align: center">Harga</th>
			<th style="text-align: center">Stok</th>
		</tr>	
	</thead>
	<?php $i=1; foreach(Komoditas::findAllKomoditasInduk() as $data) { ?>
	
	<tr>
		 <?php if(!$data->hasSub()) { ?>			 				 	
            <td><?php print $data->nama; ?></td>  
            <td style="width:20%;text-align:right">
                <?php print Helper::rp(Harga::getHarga($data->id,$model->id,$kemarin)); ?>
                <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('harga/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$kemarin),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>                    
            </td>
            <td style='text-align:right'>
                <?php print Helper::rp(Stok::getStok($data->id,$model->id,$kemarin)); ?>                    
                <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('stok/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$kemarin),array('data-toggle'=>'tooltip','title'=>'Input Stok Baru')); ?>
            </td>

            <td style="width:20%;text-align:right">
                <?php print Helper::rp(Harga::getHarga($data->id,$model->id,$hariIni)); ?>                    
                <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('harga/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$hariIni),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>                    
            </td>
            
            <td style='text-align:right'>
                <?php print Helper::rp(Stok::getStok($data->id,$model->id,$hariIni)); ?>                    
                <?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('stok/create','id_lokasi'=>$model->id,'id_komoditas'=>$data->id,'tanggal'=>$hariIni),array('data-toggle'=>'tooltip','title'=>'Input Stok Baru')); ?>
            </td>                 
        <?php } ?>   



        <?php if($data->hasSub()) { ?>
            <?php $this->renderPartial('_view',array(
                    'data'=>$data,
                    'model'=>$model,
                    'hariIni'=>$hariIni,
                    'kemarin'=>$kemarin,
                    'kemarinLusa'=>$kemarinLusa
            )); ?>
        <?php } ?>              
	</tr>

	<?php $i++; } ?>   
</table>


