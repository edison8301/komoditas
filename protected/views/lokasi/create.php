<?php
$this->breadcrumbs=array(
	'Lokasis'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Lokasi','url'=>array('index')),
array('label'=>'Manage Lokasi','url'=>array('admin')),
);
?>

<h1>Tambah Lokasi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>