<?php
$this->breadcrumbs=array(
	'Lokasi'=>array('admin'),
	'Kelola',
);
?>

<h1>Kelola Lokasi</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'lokasi-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'booster.widgets.TbButtonColumn',				
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('lokasi/create'),
		'label'=>'Tambah Lokasi',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'plus'
)); ?>&nbsp;

</div>
