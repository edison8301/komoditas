<h1>Export Data</h1>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'bahan-export-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<div class="well">
    <?php echo $form->dropDownListGroup($model,'id_komoditas',array(
        'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
            'widgetOptions'=>array(
                'htmlOptions'=>array('empty'=>'-- Pilih Komoditas --'),
                'data'=>Komoditas::getListKomoditas()
            )
    )); ?>

    <?php echo $form->dropDownListGroup($model,'id_lokasi',array(
         'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
            'widgetOptions'=>array(
                'htmlOptions'=>array('empty'=>'-- Pilih Lokasi --'),
                'data'=>CHtml::listData(Lokasi::model()->findAll(),'id','nama')
            ),
            'prepend' => '<i class="glyphicon glyphicon-map-marker"></i>'
    )); ?>

    <?php echo $form->datePickerGroup($model,'tanggal_awal',array(
            'widgetOptions' => array(
                'options' => array(
                    'format' => 'yyyy-mm-dd',
                    'autoclose'=>true
                ),
            ),
            'wrapperHtmlOptions' => array('class' => 'col-sm-3'),
            'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
    )); ?>
    <?php echo $form->datePickerGroup($model,'tanggal_akhir',array(
            'widgetOptions' => array(
                'options' => array(
                    'format' => 'yyyy-mm-dd',
                    'autoclose'=>true
                ),
            ),
            'wrapperHtmlOptions' => array('class' => 'col-sm-3'),
            'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
    )); ?>
    

</div>

<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton',array(
	        'label' => 'Export',
	        'buttonType' => 'submit',
	        'context' => 'success',
	        'icon'=>'download-alt'
	)); ?>
</div>

<?php $this->endWidget(); ?>

