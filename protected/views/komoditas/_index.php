<?php foreach($model->findAllSub() as $data) { ?>
<tr>
	<td>&nbsp;</td>
	<td>
		<?php $this->renderPartial('_button',array('data'=>$data)); ?>
		<?php print CHtml::link($data->nama,array('komoditas/view','id'=>$data->id)); ?>

	</td>
	<td style="text-align:center"><?php print $data->satuan; ?></td>
	<?php foreach(Lokasi::model()->findAll() as $lokasi) { ?>

	<td style="text-align:right">
		<?php print number_format(Harga::model()->getHarga($data->id,$lokasi->id,$tanggal),0,',','.'); ?>
		<?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('harga/create','id_lokasi'=>$lokasi->id,'id_komoditas'=>$data->id,'tanggal'=>$tanggal),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>	
	</td>	

	<td style="text-align: right">
		<?php print number_format(Stok::model()->getStok($data->id,$lokasi->id,$tanggal),0,',','.'); ?>
		<?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('stok/create','id_lokasi'=>$lokasi->id,'id_komoditas'=>$data->id,'tanggal'=>$tanggal),array('data-toggle'=>'tooltip','title'=>'Input Stok Baru')); ?>
	</td>

	<?php } ?>  

	<?php if($data->hasSub()) { ?>
	<td>&nbsp;</td>
	<?php } else { ?>
		<td style="text-align:center"><?php print $data->getRelation("status","nama"); ?></td>
	<?php } ?>

	
</tr>
<?php } ?>
