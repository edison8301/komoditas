<?php
$this->breadcrumbs=array(
	'Bahan Pokoks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List BahanPokok','url'=>array('index')),
	array('label'=>'Create BahanPokok','url'=>array('create')),
	array('label'=>'View BahanPokok','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage BahanPokok','url'=>array('admin')),
	);
	?>

	<h1>Sunting Komoditas</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>