<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'bahan-pokok-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="well">
	<?php echo $form->select2Group($model,'id_induk',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-3'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'empty'=>'-- Pilih Parent --'),
					'data'=>Komoditas::getListKomoditas()
				)
	)); ?>
	
	<?php echo $form->textFieldGroup($model,'nama',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5','maxlength'=>255)
					)
	)); ?>
	
	<?php echo $form->textFieldGroup($model,'satuan',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-2'),
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5','maxlength'=>255
				)
	))); ?>
	
	<?php echo $form->textFieldGroup($model,'urutan',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-1'),				
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'class'=>'span5'
						)
	))); ?>

	<?php echo $form->select2Group($model,'id_status',array(
		'wrapperHtmlOptions'=>array('class'=>'col-sm-2'),
		'widgetOptions'=>array(
			'data'=>Komoditas::model()->getStatus(),			
		)
	)); ?>
	</div>

	<div class="form-actions well">
		<div class="row">
			<div class="col-sm-3">&nbsp;</div>
			<div class="col-sm-9">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'success',
					'icon'=>'ok',
					'label'=>'Simpan',
				)); ?>

				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'link',
					'context'=>'success',
					'icon'=>'remove',
					'label'=>'Batal',
					'url'=>Yii::app()->request->Urlreferrer
				)); ?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>
