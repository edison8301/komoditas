<?php
$this->breadcrumbs=array(
	'Bahan Pokoks'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List BahanPokok','url'=>array('index')),
array('label'=>'Manage BahanPokok','url'=>array('admin')),
);
?>

<h1>Tambah Komoditas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>