<?php
$this->breadcrumbs=array(
	'Bahan Pokik'=>array('admin'),
	'Kelola',
);
?>

<h1>Kelola Komoditas</h1>


<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('komoditas/create'),
		'label'=>'Tambah Komoditas',
		'context'=>'success',
		'icon'=>'plus'
)); ?>&nbsp;


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'bahan-pokok-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'id',
			'id_induk',
			'urutan',
			'nama',
			'satuan',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
