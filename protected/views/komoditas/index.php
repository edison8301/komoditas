<?php
$this->pageTitle=Yii::app()->name;
?>

<?php
	date_default_timezone_set('Asia/Jakarta');
	$tanggal = date('Y-m-d');
	if(isset($_GET['tanggal'])) $tanggal = $_GET['tanggal'];
	if($tanggal > date('Y-m-d')) $tanggal = date('Y-m-d');	

	if(isset($_GET['id_lokasi'])) $id_lokasi = $_GET['id_lokasi'];

	$hariIni = date('Y-m-d');
    $kemarin = date( 'Y-m-d',strtotime('-1 days'));
    $kemarinLusa = date( 'Y-m-d',strtotime('-2 days'));
?>


<h1>Harga Komoditas di </h1>
<h2>per <?php print Helper::getTanggalSingkat($tanggal); ?></h2>
<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('komoditas/create'),
		'label'=>'Tambah komoditas',
		'context'=>'success',
		'icon'=>'plus'
	)
); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('harga/exportExcel','tanggal'=>$tanggal),
		'label'=>'Export Harga Per Tanggal',
		'context'=>'success',
		'icon'=>'download-alt'
	)
); ?>&nbsp;

<div>&nbsp;</div>

<div class="well">
	<?php print CHtml::beginForm(array('komoditas/index'),'get'); ?>
	<div class="row">
		<div class="col-md-2" style="text-align: right;padding-top: 5px">
	        <?php print CHtml::label('Tanggal',''); ?>
	    </div>

		<div class="col-md-3">			
			<?php $this->widget('booster.widgets.TbDatePicker',array(
						'name'=>'tanggal',
						'value' => $tanggal,
						'options'=>array(
			    			'showAnim'=>'fold',
			    			'format'=>'yyyy-mm-dd',
			    			'autoclose'=>true
						),
						'htmlOptions'=>array(
			    			'class'=>'form-control',
			    			'placeholder'=>'Filter Tanggal',
			    			
						),
			)); ?>			
		</div>

		<div class="col-md-2">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'success',			
					'icon'=>'search',					
				)); ?>	
		</div>
	</div>
	
	<div>&nbsp;</div>

	<div class="row">
		<div class="col-md-2" style="text-align: right;padding-top: 5px">
	        <?php print CHtml::label('Lokasi Pasar',''); ?>
	    </div>

		<div class="col-md-4">			
			<?php print CHtml::dropDownList('id_lokasi','',CHtml::listData(Lokasi::model()->findAll(),'id','nama'),array('empty'=>'-- Pilih Lokasi Pasar --','class'=>'form-control')) ?>
		</div>

		<div class="col-md-2">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'success',			
					'icon'=>'search',					
				)); ?>	
		</div>
	</div>
	<?php print CHtml::endForm(); ?>
</div>

<?php $numLokasi = Lokasi::model()->count(); $numLokasi++; ?>

<table class="table table-hover table-condensed table-bordered">
	<thead>
		<tr>
			<th rowspan="3" style="vertical-align:middle">No</th>
			<th rowspan="3" style="vertical-align:middle">Nama Bahan Pokok dan Jenisnya</th>
			<th rowspan="3" style="vertical-align:middle">Satuan</th>
			<th colspan="4" style="text-align: center">Perbandingan Harga</th>			
			<th rowspan="3" style="text-align:center;vertical-align:middle">Status</th>
		</tr>
		<tr>
			<th colspan="2" style="text-align: center"><?php print Helper::getTanggalSingkat($kemarin) ?></th>
			<th colspan="2" style="text-align: center"><?php print Helper::getTanggalSingkat($hariIni) ?></th>
			<!-- <th>Rata-rata</th> -->
		</tr>
		<tr>
			<th>harga</th>
			<th>stok</th>
			<th>harga</th>
			<th>stok</th>
		</tr>

	</thead>
	
	<tbody>
		<?php $i=1; foreach(Komoditas::findAllKomoditas() as $data) { ?>
		<tr>
			<td style="width:5%;font-weight:bold">
				<?php $this->renderPartial('_button',array('data'=>$data)); ?>
			</td>
			<td style="width:30%;font-weight:bold">
				<?php if($data->hasSub()) { ?>
				<?php print $data->nama; ?>
				<?php } else { ?>
				<?php print CHtml::link($data->nama,array('komoditas/view','id'=>$data->id));  } ?> 
			</td>

	<?php if($data->hasSub()) { ?>
		<td style="width:5%"></td>
	<?php } else { ?>
		<td style="width:5%;text-align:center"><?php print $data->satuan; ?></td>
	<?php } ?>

	<?php foreach(Lokasi::findAllLokasi() as $lokasi) { ?>
		<?php if($data->hasSub()) { ?>
			<td>&nbsp;</td>
		<?php } else { ?>

		<td style="text-align:right">
			<?php print number_format($data->getHarga($lokasi->id,$tanggal),0,',','.'); ?>
			<?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",
				array('harga/create','id_lokasi'=>$lokasi->id,'id_komoditas'=>$data->id),
				array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>	
		</td>
	<?php }} ?>

	<?php if($data->hasSub()) { ?>
		<td>&nbsp;</td>
	<?php } else { ?>	
		<td style="text-align:right">
			<?php print number_format(Harga::model()->getHargaRata($data->id,
			$tanggal),0,',','.'); ?>
		</td>
	<?php } ?>
	</tr>

<?php if($data->hasSub()) { ?>
	<?php $this->renderPartial('_index',array(
		'model'=>$data,		
		'tanggal'=>$tanggal,
		'lokasi'=>$lokasi,
		'hariIni'=>$hariIni,
		'kemarin'=>$kemarin,
		'kemarinLusa'=>$kemarinLusa
	)); ?>
<?php } ?>
<?php $i++; } ?>
	</tbody>
</table>
