<?php $this->widget('booster.widgets.TbButtonGroup',array(
		'size' => 'small',
		'context' => 'success',
		'encodeLabel'=>false,
		'buttons' => array(
			array(
				'label' => '',
				'items' => array(
             		array('label' => '<i class="glyphicon glyphicon-pencil"></i> Sunting', 'url' => array('komoditas/update','id'=>$data->id,'id_komoditas'=>$data->id)),
             		array('label' => '<i class="glyphicon glyphicon-plus"></i> SubJenis', 'url' => array('komoditas/create','id_induk'=>$data->id),'visible'=>$data->id_induk==NULL ? 1 : 0),
             		'---',
             		array('label' => '<i class="glyphicon glyphicon-arrow-up"></i> Naik Urutan', 'url' => array('komoditas/naikUrutan','id'=>$data->id)),
             		array('label' => '<i class="glyphicon glyphicon-arrow-down"></i> Turun Urutan', 'url' => array('komoditas/turunUrutan','id'=>$data->id)),
				)
			),
		)
)); ?>