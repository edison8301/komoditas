<?php
$this->breadcrumbs=array(
	'Bahan Pokoks'=>array('index'),
	$model->id,
);
?>

<h1>Lihat Bahan Pokok</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			array(
				'name' =>'id_induk',
				'value' =>$model->getParent()),
			'urutan',
			'nama',
			'satuan',
			array(
				'name'=>'id_status',
				'type'=>'raw',
				'value'=>'<div style="margin-top:5px"><span class="label label-success">'.$model->getRelation('status','nama').'</span></div>'
			)
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align: right">

	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'url'=>array('komoditas/update','id'=>$model->id),
			'label'=>'Sunting',
			'size' => 'small',
			'context'=>'success',
			'icon'=>'pencil'
	)); ?>&nbsp;
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'url'=>array('komoditas/create'),
			'label'=>'Tambah',
			'size' => 'small',
			'context'=>'success',
			'icon'=>'plus'
	)); ?>&nbsp;
	
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'url'=>array('komoditas/index'),
			'label'=>'Kelola',
			'size' => 'small',
			'context'=>'success',
			'icon'=>'list'
	)); ?>&nbsp;


</div>

<h2>Data Perubahan Harga</h2>

<?php $harga = new Harga; ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'kuosioner-grid',
		'type'=>'striped hover bordered condensed',	
		'dataProvider'=>$harga->cari($model->id),
		'filter'=>$harga,
		'ajaxUpdate'=>false,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array('style'=>'text-align:center','width'=>'30px'),
			),
			array(
				'name' =>'id_lokasi',
				'value' => '$data->getNamaLokasi()',
				'filter' => CHtml::ListData(Lokasi::model()->findAll(),"id","nama")
			),

			array(
			'name'=>'tanggal',
			'header'=>'Tanggal',
			'type'=>'raw',
			'value'=>'Helper::getTanggalSingkat($data->tanggal)',
			'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'model'=>$harga, 
						'attribute'=>'tanggal', 
						'options' => array(  
							'showOn'=>'focus',
							'dateFormat' => 'yy-mm-dd',
							'changeMonth' => true,
							'changeYear' => true,
						),
			'htmlOptions'=>array('class'=>'form-control')
			),true),
		),
			array(
				'name' => 'harga',
				'value' => '"Rp ".number_format($data->harga,"0",",",".")',
				'htmlOptions'=>array('style'=>'text-align:right')
			),
					
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template' => '{update} {delete}',
				'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
				'updateButtonUrl'=> 'Yii::app()->createUrl("harga/update", array("id"=>$data->id))',
				'deleteButtonUrl'=> 'Yii::app()->createUrl("harga/delete", array("id"=>$data->id))',
				
			),		
))); ?>
