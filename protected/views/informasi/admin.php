<?php
$this->breadcrumbs=array(
	'Informasi'=>array('index'),
	'Kelola',
);

?>

<h1>Kelola Informasi</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'informasi-grid',
	'type' => 'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			array(
				'name' => 'urutan',
				'htmlOptions'=>array(
					'style' =>'text-align: center; width: 1%')),
			'nama_singkat',
			'nama_informasi',
			array(
				'name' => 'waktu_dibuat',
				'value' => 'Helper::getCreatedTime($data->waktu_dibuat)'),
			array(
				'type'=>'raw',
				'header'=>'Naik Urutan',
				'value'=>'CHtml::link("<center><i class=\'glyphicon glyphicon-arrow-up\'></i></center>",array("informasi/naikUrutan","id"=>"$data->id"))',
			),
			array(
				'type'=>'raw',
				'header'=>'Turun Urutan',
				'value'=>'CHtml::link("<center><i class=\'glyphicon glyphicon-arrow-down\'></i></center>",array("informasi/turunUrutan","id"=>"$data->id"))',
			),			
	array(
		'class'=>'booster.widgets.TbButtonColumn',
		'template'=>'{update} {delete}'
	),
	),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('informasi/create'),
		'label'=>'Tambah Informasi',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'plus'
)); ?>&nbsp;

</div>
