<?php
$this->breadcrumbs=array(
	'Informasis'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Informasi','url'=>array('index')),
array('label'=>'Create Informasi','url'=>array('create')),
array('label'=>'Update Informasi','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Informasi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Informasi','url'=>array('admin')),
);
?>

<h1>View Informasi #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama_singkat',
		'nama_informasi',
		'urutan',
		'waktu_dibuat',
),
)); ?>
