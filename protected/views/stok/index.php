<?php
$this->breadcrumbs=array(
	'Stoks',
);

$this->menu=array(
array('label'=>'Create Stok','url'=>array('create')),
array('label'=>'Manage Stok','url'=>array('admin')),
);
?>

<h1>Stoks</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
