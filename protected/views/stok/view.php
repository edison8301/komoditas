<?php
$this->breadcrumbs=array(
	'Stoks'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Stok','url'=>array('index')),
array('label'=>'Create Stok','url'=>array('create')),
array('label'=>'Update Stok','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Stok','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Stok','url'=>array('admin')),
);
?>

<h1>View Stok #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_komoditas',
		'id_lokasi',
		'stok',
		'tanggal',
		'waktu_dibuat',
),
)); ?>
