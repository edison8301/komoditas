<?php
$this->breadcrumbs=array(
	'Stok'=>array('admin'),
	'Kelola Stok',
);
?>

<h1>Kelola Stok</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',		
		'label'=>'Tambah Stok',
		'context'=>'success',
		'icon'=>'plus',
		'url'=>array('create'),
	)
); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'stok-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>'stiped bordered condensed',
	'columns'=>array(			
			'id_komoditas',
			'id_lokasi',
			'stok',
			'tanggal',			
	array(
		'class'=>'booster.widgets.TbButtonColumn',
	),
	),
)); ?>
