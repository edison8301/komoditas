<?php
$this->breadcrumbs=array(
	'Hargas'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Harga','url'=>array('index')),
array('label'=>'Manage Harga','url'=>array('admin')),
);
?>

<h1>Tambah Harga</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>