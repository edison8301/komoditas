<?php

$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'name'=>'firstDate',
    'options'=>array('showAnim'=>'fold',
        'dateFormat'=>'yy/mm/dd',
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
    ),
));
?>