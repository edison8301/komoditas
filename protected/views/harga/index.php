<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
");

?>

<h1>Harga Komoditas per <?php print date('Y-m-d'); ?></h1>

<?php $this->widget('booster.widgets.TbButton',
	array(
		'buttonType'=>'link',
		'url'=>array('komoditas/create'),
		'label'=>'Tambah komoditas',
		'context'=>'success',
		'icon'=>'plus'
	)
); ?>&nbsp;

<?php $this->widget('booster.widgets.TbButton',
	array(
		'buttonType'=>'link',
		'url' =>'#',
		'htmlOptions'=>array(
			'class' => 'search-button'),
		'label'=>'Filter',
		'context'=>'success',
		'icon'=>'download-alt'
	)
); 
$model = new Harga;
?>&nbsp;



<?php $this->widget('booster.widgets.TbButton',
	array(
		'buttonType'=>'link',
		'url'=>array('exportExcel'),
		'label'=>'Export',
		'context'=>'success',
		'icon'=>'download-alt'
	)
); ?>&nbsp;


<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>
<!-- search-form -->

<div>&nbsp;</div>

<?php $numLokasi = Lokasi::model()->count(); $numLokasi++; ?>

<table class="table table-hover table-condensed">
	<thead>
		<tr>
			<th rowspan="2" style="vertical-align:middle">No</th>
			<th rowspan="2" style="vertical-align:middle">Nama Bahan Pokok dan Jenisnya</th>
			<th rowspan="2" style="vertical-align:middle">Satuan</th>
			<th colspan="<?php print $numLokasi; ?>" style="text-align:center">Lokasi</th>		
		</tr>
		<tr>
			<?php foreach(Lokasi::model()->findAll() as $lokasi) { ?>
				<th><?php print $lokasi->nama; ?></th>
			<?php } ?>
			<th>Rata-rata</th>
		</tr>
	</thead>
	
	<tbody>
		<?php $i=1; foreach(Komoditas::model()->findAllByAttributes(array('id_induk'=>null),array('order'=>'urutan ASC')) as $data) { ?>
		<tr>
			<td style="width:5%;font-weight:bold">
				<?php 
					$this->widget(
					    'booster.widgets.TbButtonGroup',
					    array(
					        'size' => 'small',
					        'context' => 'success',
					        'buttons' => array(
					            array(
					                'label' => '',
					                'items' => array(
             							 array('label' => 'Update Bahan', 'url' => array('komoditas/update','id'=>$data->id,'id_komoditas'=>$data->id)),
             							 array('label' => 'Tambah Sub Jenis', 'url' => array('komoditas/create','id_induk'=>$data->id)),
             							 '---',
             							  array('label' => 'Naik Urutan', 'url' => array('komoditas/naikUrutan','id'=>$data->id)),
             							  array('label' => 'Turun Urutan', 'url' => array('komoditas/turunUrutan','id'=>$data->id)),
					                )
					            ),
					        ),
					    )
					); echo ' ';
				 ?>
			</td>


			

			<td style="width:30%;font-weight:bold">
				<?php print $data->nama; ?>
			</td>

	<?php if($data->hasSub()) { ?>
		<td style="width:5%">&nbsp;</td>
	<?php } else { ?>
		<td style="width:5%;text-align:center"><?php print $data->satuan; ?></td>
	<?php } ?>

	<?php foreach(Lokasi::model()->findAll() as $lokasi) { ?>
		<?php if($data->hasSub()) { ?>
			<td>&nbsp;</td>
		<?php } else { ?>

		<td style="text-align:right">
			<?php print number_format($data->getHarga($lokasi->id,date('Y-m-d')),0,',','.'); ?>
			<?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",
				array('harga/create','id_lokasi'=>$lokasi->id,'id_komoditas'=>$data->id),
				array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>	
		</td>
	<?php }} ?>

	<?php if($data->hasSub()) { ?>
		<td>&nbsp;</td>
	<?php } else { ?>	
		<td style="text-align:right">
			<?php print number_format(Harga::model()->getHargaRata($data->id,
			date('Y-m-d')),0,',','.'); ?>
		</td>
	<?php } ?>
	</tr>

<?php if($data->hasSub()) { ?>
	<?php $this->renderPartial('_tabel',array('model'=>$data->getSub())); ?>

<?php } ?>
<?php $i++; } ?>
	</tbody>
</table>