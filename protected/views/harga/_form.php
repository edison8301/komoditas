<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'harga-form',
	'type' => 'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<div class="well">
	<?php echo $form->dropDownListGroup($model,'id_komoditas',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-3'),
				'widgetOptions'=>array(
					'htmlOptions'=>array('empty'=>'-- Pilih Bahan Pokok --','disabled'=>'disabled'),
					'data'=>CHtml::listData(Komoditas::model()->findAll(),'id','nama')
				)
	)); ?>
	
	<?php echo $form->select2Group($model,'id_lokasi',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-3'),
				'widgetOptions'=>array(
					'htmlOptions'=>array('empty'=>'-- Pilih Lokasi --'),
					'data'=>CHtml::listData(Lokasi::model()->findAll(),'id','nama')
				)
	)); ?>
	
	<?php echo $form->datePickerGroup($model,'tanggal',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-3'),
				'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
				), 
				'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 
	)); ?>

	<?php echo $form->textFieldGroup($model,'harga', array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-3'),
			'widgetOptions'=>array(
				'htmlOptions'=>array('rows'=>3,'class'=>'format-money')
			),
			'prepend'=>'Rp',
			'label'=>'Harga'
	)); ?>

</div>

	<div class="form-actions well">
		<div class="row">
			<div class="col-sm-3">&nbsp;</div>
			<div class="col-sm-9">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'success',
					'icon'=>'ok',
					'label'=>'Simpan',
				)); ?>

				
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'link',
					'context'=>'success',
					'icon'=>'remove',
					'label'=>'Batal',
					'url'=>Yii::app()->request->Urlreferrer
				)); ?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>
