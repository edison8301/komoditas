<?php foreach($model as $data) { ?>
<tr>
	<td>&nbsp;</td>
	<td>
		<?php 
			$this->widget(
			    'booster.widgets.TbButtonGroup',
			    array(
			        'size' => 'small',
			        'context' => 'success',
			        'buttons' => array(
			            array(
			                'label' => '',
			                'items' => array(
											array('label' => 'Update Bahan', 'url' => array('komoditas/update','id'=>$data->id,'id_komoditas'=>$data->id)),	
											'---',	
											  array('label' => 'Naik Urutan', 'url' => array('komoditas/naikUrutan','id'=>$data->id)),
											   array('label' => 'Turun Urutan', 'url' => array('komoditas/turunUrutan','id'=>$data->id)),						
			                )
			            ),
			        ),
			    )
			); echo ' ';
		 ?>
		<?php print $data->nama; ?>

	</td>
	<td style="text-align:center"><?php print $data->satuan; ?></td>
	<?php foreach(Lokasi::model()->findAll() as $lokasi) { ?>
	<td style="text-align:right"><?php print number_format(Harga::model()->getHarga($data->id,$lokasi->id,$tanggal),0,',','.'); ?>
		<?php print CHtml::link("<i class='glyphicon glyphicon-pencil icon'></i>",array('harga/create','id_lokasi'=>$lokasi->id,'id_komoditas'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Input Harga Baru')); ?>
	</td>
	<?php } ?>
	<?php if($data->hasSub()) { ?>
	<td>&nbsp;</td>
	<?php } else { ?>
	<td style="text-align:right"><?php print number_format(Harga::model()->getHargaRata($data->id,$tanggal),0,',','.'); ?></td>
	<?php } ?>
	
</tr>
<?php } ?>