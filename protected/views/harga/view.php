<?php
$this->breadcrumbs=array(
	'Hargas'=>array('index'),
	$model->id,
);
?>

<h1>Lihat Harga</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('update','id'=>$model->id),
		'label'=>'Sunting',
		'context'=>'primary',
		'icon'=>'pencil'
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('create'),
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus'
)); ?>&nbsp;

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			'id',
			'id_bahan',
			'id_lokasi',
			'harga',
			'tanggal',
		),
)); ?>
