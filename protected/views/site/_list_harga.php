<div class="panel panel-info">
    <div class="panel-heading">DATA KOMODITAS</div>

        <table class="table">
            <thead class="style-head">
            <tr style="border: 2px solid white">      
                <th style="width:50%;text-align:left;">Nama Komoditas</th>  
                <th style="width:25%;text-align:center;" class="text-center">Satuan</th>
                <th style="width:25%;text-align:center;">Harga Rata-rata</th>
            </tr>
            </thead>
        </table>
        <!-- END HEAD TABLE -->

        <!-- START BODY TABLE -->
        <div id="efek_tabel" class="style-table">
        <?php $i=1; foreach(Komoditas::findAllKomoditasInduk() as $data) { ?>
            <table class="table table bordered">
            <tr class="style-table">
                <td style="width:50%;font-weight:bold"><?php print $data->nama; ?></td>
                <td style="width:25%;" class="text-center"><?php print $data->getSatuan(); ?></td>
                <td style="width:25%;text-align:right">
                  <?php if(!$data->hasSub()) { ?>
                  <?php print number_format(Harga::model()->getHargaRata($data->id,date('Y-m-d')),0,',','.'); ?>
                  <?php } ?>
                </td>
            </tr>
            <?php if($data->hasSub()) { ?>
                <?php $this->renderPartial('_tabel',array('model'=>$data->getSub())); ?>
            <?php } ?>
            </table>
        <?php } ?>
        </div>
    
</div>

<!-- #efek_tabel -->

<script>
  $(document).ready(function()
    {
      $('#efek_tabel').show();
      $('#efek_logo').show();
    })
</script>

<?php $this->widget('ext.carouFredSel.ECarouFredSel', array(
      'id' => 'carousel',
      'target' => '#efek_tabel',
      'config' => array(
        'items' => 1,
        'height' => null,
        'width' => '100%',
        'direction' => 'up',
      ),
  )); 
?>