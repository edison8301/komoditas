<!-- PANGGIL EXTENSIONS JS CHART-->
<script type="text/javascript" 
  src="<?php echo Yii::app()->request->baseUrl; ?>/vendors/fusioncharts/js/fusioncharts.js">
</script>
<script type="text/javascript" 
  src="<?php echo Yii::app()->request->baseUrl; ?>/vendors/fusioncharts/js/themes/fusioncharts.theme.fint.js">
</script>



<!-- SCRIPT FUSION CHART -->
<script>
  FusionCharts.ready(function(){
  var revenueChart = new FusionCharts({
    "type": "line",
    "renderAt": "chartContainer",
    "width": "80%",
    "height": "35%",
    "dataFormat": "json",
    "dataSource": {
      "chart": {
        "caption": "Statistik Pergerakan Harga",
        "subCaption": "Bahan Pokok",
        "xAxisName": "Tanggal",
        "yAxisName": "Harga",
        "paletteColors": "#1936E4",
        "baseFontColor": "#000000",
        "baseFont": "Helvetica Neue,Arial",
        "baseFontSize": "15",
        "captionFontSize": "25",
        "subcaptionFontSize": "15",
        "subcaptionFontBold": "0",
        "numberPrefix": "Rp. ",
        "canvasBgAlpha": "0",
        "bgColor": "2C8CD4,1C3FE3",
        "bgratio": "60,40",
        "bgAlpha": "90,80",
        "showShadow": "0",
        "canvasBgColor": "#ffffff",
        "canvasBorderAlpha": "0",
        "divlineAlpha": "60",
        "divlineColor": "#000",
        "divlineThickness": "2",

        "anchorRadius": "4",
        "anchorBorderThickness": "1",
        "anchorBgColor": "#34B5CC",

        "divLineDashed": "1",
        "divLineDashLen": "1",
        "divLineGapLen": "0",

        "formatNumberScale": "0",
        "decimalSeparator": ",",
        "thousandSeparator": ".",
        "linethickness": "10",
        "showXAxisLine": "1",
        "xAxisNameFontSize": "15",
        "yAxisNameFontSize": "15",
        "xAxisLineThickness": "1",
        "xAxisLineColor": "#999999",
        "showValues": "0",

        "theme": "fint"
        
      },
      "data": [ <?php print Harga::getGrafik(1,4);?> ]
    }
  });
    revenueChart.render();
  })
</script>   

      <div class="col-md-12 text-center">
        <div id="chartContainer" class="jarak"></div>
      </div>


  <script>
  $(document).ready(function()
    {
      $('#chartContainer').show();
      $('#efek_logo').show();
    })
</script>

<?php
  $this->widget('ext.carouFredSel.ECarouFredSel', array(
      'id' => 'carousel',
      'target' => '#chartContainer',
      'config' => array(
        'items' => 1,
        'height' => '100',
        'direction' => 'up',
      ),
  )); 
?>