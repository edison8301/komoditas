<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>


<div class="row">
	<?php /*
	<div class="col-xs-3" style="text-align:center">
		<div >
			<img class="foto" style="" src="<?php print Yii::app()->request->baseUrl; ?>/images/foto.jpg">
		</div>
		
		<style>
			.foto {
				width:120px;border:1px solid #666;padding:3px;
				-webkit-box-shadow: 0 0 3px 2px rgba(0,0,0,0.2);
box-shadow: 0 0 3px 2px rgba(0,0,0,0.2);
			}
		</style>
	</div>
	*/ ?>
	<div class="col-xs-12">
		<h3 style="text-align:center">Daftar Isian Harga Beberapa Bahan Pokok<br>
		Yang Ada di Pasar Kota Serang<br>
		Hari: <span id="datetime"><?php //print Helper::getCreatedDate(date('Y-m-d')); ?></span>
		</h3>
		<div id="datetimes"></div>
		<script type="text/javascript">window.onload = date_time('datetime');</script>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/datetime.js"); ?>
	</div>
</div>


<div>&nbsp;</div>

<?php $numLokasi = Lokasi::model()->count(); $numLokasi++; ?>
<table class="table table-hover table-bordered">
<thead>
<tr>
	<th rowspan="2" style="vertical-align:middle">No</th>
	<th rowspan="2" style="vertical-align:middle">Nama Bahan Pokok dan Jenisnya</th>
	<th rowspan="2" style="vertical-align:middle;text-align:center">Satuan</th>
	<th colspan="<?php print $numLokasi; ?>" style="text-align:center">Lokasi</th>
</tr>
<tr>
	<?php foreach(Lokasi::model()->findAll() as $lokasi) { ?>
	<th style="text-align:center"><?php print $lokasi->nama; ?></th>
	<?php } ?>
	<th style="text-align:center">Rata-rata</th>
</tr>

</thead>
<tbody>

<?php $i=1; foreach(Bahan::model()->findAllByAttributes(array('id_induk'=>null),array('order'=>'urutan ASC')) as $data) { ?>
<tr>
	<td style="width:5%;font-weight:bold"><?php print $i; ?></td>
	<td style="width:30%;font-weight:bold">
		<?php print $data->nama; ?>
	</td>
	<?php if($data->hasSub()) { ?>
	<td style="width:5%">&nbsp;</td></td>
	<?php } else { ?>
	<td style="width:5%;text-align:center"><?php print $data->satuan; ?></td>
	<?php } ?>
	<?php foreach(Lokasi::model()->findAll() as $lokasi) { ?>
	<?php if($data->hasSub()) { ?>
	<td>&nbsp;</td>
	<?php } else { ?>
	<td style="text-align:right">
		<?php print number_format(Harga::model()->getHarga($data->id,$lokasi->id,date('Y-m-d')),0,',','.'); ?>
	</td>
	<?php } ?>
	<?php } ?>
	<?php if($data->hasSub()) { ?>
	<td>&nbsp;</td>
	<?php } else { ?>
	<td style="text-align:right"><?php print number_format(Harga::model()->getHargaRata($data->id,date('Y-m-d')),0,',','.'); ?></td>
	<?php } ?>
</tr>
<?php if($data->hasSub()) { ?>
<?php $this->renderPartial('_tabel',array('model'=>$data->getSub())); ?>
<?php } ?>
<?php $i++; } ?>
</tbody>
</table>

<div>&nbsp;</div>