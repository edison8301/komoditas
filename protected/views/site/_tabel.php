<?php foreach($model as $data) { ?>
<tr>
	<td style="width:50%;"><?php print $data->nama; ?></td>
	<td style="width:25%;text-align:center"><?php print $data->getSatuan(); ?></td>
	<td style="width:25%;text-align:right">
		<?php print number_format(Harga::model()->getHargaRata($data->id,date('Y-m-d')),0,',','.'); ?>
	</td>
</tr>
<?php } ?>