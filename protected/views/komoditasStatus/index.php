<?php
$this->breadcrumbs=array(
	'Komoditas Statuses',
);

$this->menu=array(
array('label'=>'Create KomoditasStatus','url'=>array('create')),
array('label'=>'Manage KomoditasStatus','url'=>array('admin')),
);
?>

<h1>Komoditas Statuses</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
