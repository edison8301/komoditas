<?php
$this->breadcrumbs=array(
	'Komoditas Statuses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List KomoditasStatus','url'=>array('index')),
	array('label'=>'Create KomoditasStatus','url'=>array('create')),
	array('label'=>'View KomoditasStatus','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage KomoditasStatus','url'=>array('admin')),
	);
	?>

	<h1>Update KomoditasStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>