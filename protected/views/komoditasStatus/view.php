<?php
$this->breadcrumbs=array(
	'Komoditas Statuses'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List KomoditasStatus','url'=>array('index')),
array('label'=>'Create KomoditasStatus','url'=>array('create')),
array('label'=>'Update KomoditasStatus','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete KomoditasStatus','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage KomoditasStatus','url'=>array('admin')),
);
?>

<h1>View KomoditasStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
