<?php
$this->breadcrumbs=array(
	'User'=>array('admin'),
	'Kelola',
);
?>

<h1>Kelola User</h1>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'user-grid',
		'type' => 'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
				'username',
				array(
					'name' => 'id_role',
					'value' => '$data->getRole()',
					'filter' => CHtml::listData(Role::model()->findAll(),'id','nama')),
		array(
			'class'=>'booster.widgets.TbButtonColumn',
			'template'=>'{update} {delete}'
		),
),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('user/create'),
		'label'=>'Tambah User',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'plus'
)); ?>&nbsp;

</div>
