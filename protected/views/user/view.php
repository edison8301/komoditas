<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List User','url'=>array('index')),
array('label'=>'Create User','url'=>array('create')),
array('label'=>'Update User','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete User','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage User','url'=>array('admin')),
);
?>

<h1>User <?php echo $model->username; ?></h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered condensed',
'attributes'=>array(
		'username',
		'password',
		array(
			'name' => 'id_role',
			'value' => $model->getRole())
),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('user/create'),
		'label'=>'Tambah User',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'plus'
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('user/update','id'=>$model->id),
		'label'=>'Sunting User',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'pencil'
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'url'=>array('user/admin'),
		'label'=>'Kelola User',
		'size'=>'small',
		'context'=>'success',
		'icon'=>'pencil'
)); ?>&nbsp;

</div>
