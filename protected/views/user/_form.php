<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
		<?php echo $form->textFieldGroup($model,'username',array(
					'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
					'widgetOptions'=>array(
						'htmlOptions'=>array(
							'class'=>'span5','maxlength'=>255
		)))); ?>
		
		<?php if(Yii::app()->controller->id=="user" AND Yii::app()->controller->action->id=="create") { ?>
			<?php echo $form->passwordFieldGroup($model,'password',array(
						'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
						'widgetOptions'=>array(
							'htmlOptions'=>array(
								'class'=>'span5','maxlength'=>255
			)))); ?>
		<?php } ?>
	
		<?php echo $form->dropDownListGroup($model,'id_role',array(
					'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
					'widgetOptions'=>array(
						'data'=>CHtml::listData(Role::model()->findAll(),'id','nama'),
						'htmlOptions'=>array(
							'class'=>'span5'
		)))); ?>
	</div>
	
	<div class="form-actions well">
		<div class="row">
			<div class="col-sm-3">&nbsp;</div>
			<div class="col-sm-9">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'success',
					'icon'=>'ok',
					'label'=>'Simpan',
				)); ?>

				
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'link',
					'context'=>'success',
					'icon'=>'remove',
					'label'=>'Batal',
					'url'=>Yii::app()->request->Urlreferrer
				)); ?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>
