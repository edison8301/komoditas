-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2018 at 01:17 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `komoditas`
--

-- --------------------------------------------------------

--
-- Table structure for table `gambar`
--

CREATE TABLE `gambar` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gambar`
--

INSERT INTO `gambar` (`id`, `id_kategori`, `nama`, `gambar`) VALUES
(1, 1, 'Banner 1', 'banner-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id` int(11) NOT NULL,
  `id_komoditas` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `harga` decimal(20,0) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `waktu_dibuat` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id`, `id_komoditas`, `id_lokasi`, `harga`, `tanggal`, `waktu_dibuat`) VALUES
(1, 21, 1, '227000', '2016-01-07', NULL),
(2, 22, 1, '207000', '2016-02-18', NULL),
(3, 22, 1, '207000', '2016-01-07', NULL),
(4, 23, 1, '165000', '2016-01-07', NULL),
(5, 24, 1, '13000', '2016-01-07', NULL),
(6, 25, 1, '27000', '2016-01-07', NULL),
(7, 26, 1, '12000', '2016-01-07', NULL),
(8, 27, 1, '7000', '2016-01-07', NULL),
(9, 28, 1, '130000', '2016-01-07', NULL),
(10, 29, 1, '42000', '2016-01-07', NULL),
(11, 30, 1, '55000', '2016-01-07', NULL),
(12, 31, 1, '1500', '2016-01-07', NULL),
(13, 32, 1, '2500', '2016-01-07', NULL),
(14, 33, 1, '13000', '2016-01-07', NULL),
(15, 34, 1, '40000', '2016-01-07', NULL),
(16, 35, 1, '8000', '2016-01-07', NULL),
(17, 36, 1, '82000', '2016-01-07', NULL),
(18, 37, 1, '9000', '2016-01-07', NULL),
(19, 7, 1, '7000', '2016-01-07', NULL),
(20, 38, 1, '2000', '2016-01-07', NULL),
(21, 39, 1, '9000', '2016-01-07', NULL),
(22, 41, 1, '7500', '2016-01-07', NULL),
(23, 42, 1, '38000', '2016-01-07', NULL),
(24, 11, 1, '28000', '2016-01-07', NULL),
(25, 44, 1, '46000', '2016-01-07', NULL),
(26, 45, 1, '46000', '2016-01-07', NULL),
(27, 46, 1, '60000', '2016-01-07', NULL),
(28, 13, 1, '24000', '2016-01-07', NULL),
(29, 14, 1, '10000', '2016-01-07', NULL),
(30, 15, 1, '6000', '2016-01-07', NULL),
(31, 47, 1, '60000', '2016-01-07', NULL),
(32, 17, 1, '20000', '2016-01-07', NULL),
(33, 48, 1, '18000', '2016-01-07', NULL),
(34, 49, 1, '55000', '2016-01-07', NULL),
(35, 19, 1, '185000', '2016-01-07', NULL),
(36, 50, 1, '2000', '2016-01-07', NULL),
(37, 21, 2, '12000', '2016-01-11', NULL),
(38, 22, 2, '12000', '2016-01-11', NULL),
(39, 23, 2, '10500', '2016-01-11', NULL),
(40, 24, 2, '12000', '2016-01-11', NULL),
(41, 25, 2, '16000', '2016-01-11', NULL),
(42, 26, 2, '13000', '2016-01-11', NULL),
(43, 27, 2, '7000', '2016-01-11', NULL),
(44, 28, 2, '135000', '2016-01-11', NULL),
(45, 29, 2, '40000', '2016-01-11', NULL),
(46, 30, 2, '50000', '2016-01-11', NULL),
(47, 31, 2, '1600', '2016-01-11', NULL),
(48, 32, 2, '2500', '2016-01-11', NULL),
(49, 33, 2, '9000', '2016-01-11', NULL),
(50, 34, 2, '42000', '2016-01-11', NULL),
(51, 35, 2, '8500', '2016-01-11', NULL),
(52, 36, 2, '82000', '2016-01-11', NULL),
(53, 37, 2, '9000', '2016-01-11', NULL),
(54, 38, 2, '2000', '2016-01-11', NULL),
(55, 7, 2, '9000', '2016-01-11', NULL),
(56, 39, 2, '7500', '2016-01-11', NULL),
(57, 41, 2, '7500', '2016-01-11', NULL),
(58, 42, 2, '38000', '2016-01-11', NULL),
(59, 11, 2, '28000', '2016-01-11', NULL),
(60, 44, 2, '50000', '2016-01-11', NULL),
(61, 45, 2, '42000', '2016-01-11', NULL),
(62, 46, 2, '60000', '2016-01-11', NULL),
(63, 13, 2, '24000', '2016-01-11', NULL),
(64, 14, 2, '9000', '2016-01-11', NULL),
(65, 15, 2, '4000', '2016-01-11', NULL),
(66, 47, 2, '70000', '2016-01-11', NULL),
(67, 17, 2, '20000', '2016-01-11', NULL),
(68, 48, 2, '25000', '2016-01-11', NULL),
(69, 50, 2, '2000', '2016-01-11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE `informasi` (
  `id` int(10) NOT NULL,
  `nama_singkat` varchar(255) DEFAULT NULL,
  `nama_informasi` text NOT NULL,
  `urutan` int(11) DEFAULT NULL,
  `waktu_dibuat` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `informasi`
--

INSERT INTO `informasi` (`id`, `nama_singkat`, `nama_informasi`, `urutan`, `waktu_dibuat`) VALUES
(2, 'Selamat Datang', 'Selamat Datang di Website Komoditas Harga Bahan ', 1, '2016-02-26 10:47:51'),
(3, 'Informasi Dua', 'Dihimbau pada konsumen pasar agar berhati-hati saat memilih makan dan minuman yang mengandung asam karena akan mengakibatkan asam lambung berlebih, terimakasih', 2, '2016-02-26 11:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `komoditas`
--

CREATE TABLE `komoditas` (
  `id` int(11) NOT NULL,
  `id_induk` int(11) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `satuan` varchar(255) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komoditas`
--

INSERT INTO `komoditas` (`id`, `id_induk`, `urutan`, `nama`, `satuan`, `id_status`) VALUES
(1, NULL, 1, 'Beras', '', NULL),
(2, NULL, 2, 'Gula Pasir', '', NULL),
(3, NULL, 3, 'Minyak Goreng', '', NULL),
(4, NULL, 4, 'Daging', '', NULL),
(5, NULL, 5, 'Telur Ayam', '', NULL),
(6, NULL, 6, 'Susu', '', NULL),
(7, NULL, 7, 'Jagung Pipilan', 'Kg', 1),
(8, NULL, 8, 'Garam Beryodium', '', NULL),
(9, NULL, 9, 'Tepung Terigu', '', NULL),
(10, NULL, 10, 'Bawang Merah (Brebes)', '', NULL),
(11, NULL, 11, 'Bawang Putih', 'Kg', 1),
(12, NULL, 12, 'Cabe Merah', '', NULL),
(13, NULL, 13, 'Kacang Tanah (belum di kupas)', 'Kg', 1),
(14, NULL, 14, 'Kacang Kedelai (import)', 'Kg', 1),
(15, NULL, 15, 'Ketela Pohon', 'Kg', 1),
(16, NULL, 16, 'Ikan Asin', '', NULL),
(17, NULL, 17, 'Kacang Hijau', 'Kg', 1),
(18, NULL, 18, 'Ikan', '', NULL),
(19, NULL, 19, 'Gas LPG', '12 Kg', 1),
(20, NULL, 20, 'Mie Instant', '', NULL),
(21, 1, 1, 'Beras Permata', 'Kg', 1),
(22, 1, 2, 'Beras AA Kapal', 'Kg', 1),
(23, 1, 3, 'Beras Bunga Matahari', 'Kg', 1),
(24, 2, 1, 'Gula Pasir Lokal', 'Kg', 1),
(25, 3, 1, 'Minyak Bimoli Botol Biasa 1 Ltr', 'Botol ( 1 L )', 1),
(26, 3, 2, 'Minyak Fortune Biasa 1 Ltr', '1 (L)', 1),
(27, 3, 3, 'Minyak Gurah', '1 Botol (600 ml)', 1),
(28, 4, 1, 'Daging Sapi (Murni/Has)', 'Kg', 1),
(29, 4, 2, 'Daging Ayam Broiler', 'Kg', 1),
(30, 4, 3, 'Daging Ayam Kampung', 'Kg', 1),
(31, 5, 1, 'Telur Ayam Ras', 'Butir', 1),
(32, 5, 2, 'Telur Ayam Kampung', 'Butir', 1),
(33, 6, 1, 'Kental Manis (Bendera)', '385 gram/Kaleng', 1),
(34, 6, 2, 'Bubuk(Bendera)', '400 gram/Kotak', 1),
(35, 6, 3, 'Kental Manis Cap Omela', '385 gram/Kaleng', 1),
(36, 6, 4, 'Bubuk (Dancow)', '800 gram/Kotak', 1),
(37, 6, 5, 'Kental Manis Cap Carnation', '385 gram/Kaleng', 1),
(38, 8, 1, 'Garam Beryodium Halus', 'Bungkus', 1),
(39, 9, 1, 'Tepung Terigu Segitigas Biru', 'Kg', 1),
(40, 9, 2, 'Tepung Terigu Cakra Kembar', 'Kg', 1),
(41, 9, 3, 'Tepung Terigu Kunci', 'Kg', 1),
(42, 10, 1, 'Lokal (brebes)', 'Kg', 1),
(43, 10, 2, 'Import', 'Kg', 1),
(44, 12, 1, 'Keriting', 'Kg', 1),
(45, 12, 2, 'Raw it Jakarta', 'Kg', 1),
(46, 12, 3, 'Raw it Belitung', 'Kg', 1),
(47, 16, 1, 'Ikan Asin Teri', 'Kg', 1),
(48, 18, 1, 'Ikam Kembung', 'Kg', 1),
(49, 18, 2, 'Tenggiri', 'Kg', 1),
(50, 20, 1, 'Indomie Rasa Kari Ayam', 'Bungkus', 1);

-- --------------------------------------------------------

--
-- Table structure for table `komoditas_status`
--

CREATE TABLE `komoditas_status` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komoditas_status`
--

INSERT INTO `komoditas_status` (`id`, `nama`) VALUES
(1, 'Aktif'),
(2, 'Tidak Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id`, `nama`) VALUES
(1, 'Pasar 1'),
(2, 'Pasar 2');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `nama`) VALUES
(1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id` int(11) NOT NULL,
  `id_komoditas` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `stok` decimal(20,0) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `waktu_dibuat` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `id_role`) VALUES
(1, 'admin', '$2y$13$ZD/EuN8GuEizBc0Akv0LUOg0gnbFAVrjbk0iBmDfY3PaZbTzQasn6', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informasi`
--
ALTER TABLE `informasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komoditas`
--
ALTER TABLE `komoditas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komoditas_status`
--
ALTER TABLE `komoditas_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `harga`
--
ALTER TABLE `harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `informasi`
--
ALTER TABLE `informasi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `komoditas`
--
ALTER TABLE `komoditas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `komoditas_status`
--
ALTER TABLE `komoditas_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
