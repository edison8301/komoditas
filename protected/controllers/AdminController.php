<?php

class AdminController extends Controller
{
	public function actionIndex()
	{
		$this->layout = '//layouts/admin/column2';

		$model = new KomoditasChartForm;

		$criteria = new CDbCriteria;
	    $criteria->addCondition('id_induk IS NOT NULL');
    	$criteria->order = 'urutan ASC';

    	$model->komoditas = Komoditas::model()->find($criteria)->id;
    
    	$criteria = new CDbCriteria;
    	$criteria->order = 'id ASC';
    	$model->lokasi = Lokasi::model()->find($criteria)->id;
		
		if(isset($_POST['KomoditasChartForm']))
		{
			$model->attributes = $_POST['KomoditasChartForm'];
		}

		$this->render('index',array(
			'model'=>$model
		));
	}


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
