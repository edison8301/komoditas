<?php

class GambarController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id_kategori)
	{
        $model=new Gambar;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Gambar']))
        {
            $model->attributes=$_POST['Gambar'];

            $gambar = CUploadedFile::getInstance($model,'gambar');

            if ($gambar!==null)
            {
                $model->gambar = str_replace(' ','-',time().'_'.$gambar->name);
            }

            if($model->save())
            {
                if($gambar!==null)
                {
                    $path = Yii::app()->basePath.'/../uploads/gambar/';
                    $gambar->saveAs($path.$model->gambar);
                }

                Yii::app()->user->setFlash('success','Gambar berhasil diupload');                
                $this->redirect(array('view','id'=>$model->id));
            }

        }

        $this->render('create',array(
            'model'=>$model,
            'id_kategori' => $id_kategori
        ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $model=$this->loadModel($id);

        $old_gambar = $model->gambar;        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Gambar']))
        {
        
            $model->attributes=$_POST['Gambar'];   
            $gambar = CUploadedFile::getInstance($model,'gambar');
            
            if(is_object($gambar)) {
                $model->gambar = str_replace(' ','-',time().'_'.$gambar->name);                
            } else {
                $model->gambar = $old_gambar;
            }

            if($model->save())
            {   

                if($gambar!==null)
                {
                    $path = Yii::app()->basePath.'/../uploads/gambar/';
                    $gambar->saveAs($path.$model->gambar);
                    
                    if(file_exists($path.$old_gambar) AND $old_gambar!='')
                        unlink($path.$old_gambar);
                }

                Yii::app()->user->setFlash('success','Gambar berhasil disunting');
                $this->redirect(array('view','id'=>$model->id));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Gambar');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id_kategori)
	{
		$model=new Gambar('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Gambar']))
			$model->attributes=$_GET['Gambar'];

		$this->render('admin',array(
			'model'=>$model,
			'id_kategori' => $id_kategori
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Gambar the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Gambar::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Gambar $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gambar-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
