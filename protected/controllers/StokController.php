<?php

class StokController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Stok;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_GET['id_lokasi']))
			$model->id_lokasi = $_GET['id_lokasi'];
		
		if(isset($_GET['id_komoditas']))
			$model->id_komoditas = $_GET['id_komoditas'];
		
		$model->tanggal = date('Y-m-d');
		if(isset($_GET['tanggal']))
			$model->tanggal = $_GET['tanggal'];

		


		if(isset($_POST['Stok']))
		{
			$model->attributes=$_POST['Stok'];			
			
			date_default_timezone_get('Asia/Jakarta');
            $model->waktu_dibuat = date('Y-m-d H:i:s');

			$model->stok = str_replace('.','', $model->stok);
			$lama = Stok::model()->findByAttributes(array(
						'id_komoditas'=>$model->id_komoditas,
						'id_lokasi'=>$model->id_lokasi,
						'tanggal'=>$model->tanggal
			));
			
			if($lama!==null)
			{
				$lama->stok = $model->stok;
				$lama->save();
			} else {
				$model->save();
			}
			
			Yii::app()->user->setFlash('success', 'Data stok berhasil disimpan');
			
			if (Yii::app()->controller->id == "lokasi") {
				$this->redirect(array('lokasi/admin'));
			}

			$this->redirect(array('komoditas/index'));

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Stok']))
		{
			$model->attributes=$_POST['Stok'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Stok');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Stok('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Stok']))
			$model->attributes=$_GET['Stok'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Stok the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Stok::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Stok $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='stok-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
