<?php

class KomoditasController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','naikUrutan','turunUrutan'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','export','exportExcel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/	
	public function actionCreate()
	{
		$model = new Komoditas;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_GET['id_induk']))
			$model->id_induk = $_GET['id_induk'];
		
		if(isset($_POST['Komoditas']))
		{
			$model->attributes=$_POST['Komoditas'];
			
			if($model->urutan==null)
				$model->setUrutan();
		
			if($model->save())
			{
				$model->aturUrutan();
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('komoditas/index'));
			}
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
		
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);		
		if(isset($_POST['Komoditas']))
		{
			$model->attributes=$_POST['Komoditas'];
			if($model->save())
			{
				$model->aturUrutan();
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('komoditas/index'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
	$dataProvider=new CActiveDataProvider('Komoditas');
	$this->render('index',array(
		'dataProvider'=>$dataProvider,
	));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
	$model=new Komoditas('search');
	$model->unsetAttributes();  // clear any default values
	if(isset($_GET['Komoditas']))
	$model->attributes=$_GET['Komoditas'];

	$this->render('admin',array(
		'model'=>$model,
	));
}

	public function actionNaikUrutan($id)
	{
		$model = $this->loadModel($id);
		if($model->urutan==1)
		{
			Yii::app()->user->setFlash('danger','Urutan data terkait sudah teratas');
			$this->redirect(array('komoditas/index'));
		} else {
			$model_2 = Komoditas::model()->findByAttributes(array('id_induk'=>$model->id_induk,'urutan'=>$model->urutan-1));
			
			$model->urutan--;
			$model->save();
			
			$model_2->urutan++;
			$model_2->save();
			
			$model->aturUrutan();
			
			Yii::app()->user->setFlash('success','Urutan data berhasil naik');
			$this->redirect(array('komoditas/index'));
		}
	}

	public function actionTurunUrutan($id)
	{
		$model = $this->loadModel($id);
		
		$jumlahRincian = Komoditas::model()->countByAttributes(array('id_induk'=>$model->id_induk));
		
		if($jumlahRincian == $model->urutan)
		{
			Yii::app()->user->setFlash('danger','Urutan data terkait sudah terbawah');
			$this->redirect(array('komoditas/index'));
		} else {
		
			$model_2 = Komoditas::model()->findByAttributes(array('id_induk'=>$model->id_induk,'urutan'=>$model->urutan+1));
			
			$model->urutan++;
			$model->save();
			$model_2->urutan--;
			$model_2->save();
			
			$model->aturUrutan();
			
			Yii::app()->user->setFlash('success','Urutan data berhasil turun');
			$this->redirect(array('komoditas/index'));
		}
		
		
	}

	public function actionExport()
	{
		$this->layout = '//layouts/admin/column2';

		$model = new KomoditasExportForm;

		if(isset($_POST['KomoditasExportForm']))
		{
			$model->attributes = $_POST['KomoditasExportForm'];

			if($model->validate())
			{
				$this->redirect(array(
					'komoditas/exportExcel',
					'id_komoditas'=>$model->id_komoditas,
					'id_lokasi'=>$model->id_lokasi,
					'tanggal_awal'=>$model->tanggal_awal,
					'tanggal_akhir'=>$model->tanggal_akhir
				));
			}
		}
		
		$this->render('export',array(
			'model'=>$model
		));

	}

 
	public function actionExportExcel()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		Yii::import('application.vendors.PHPExcel',true);		
		spl_autoload_register(array('YiiBase', 'autoload'));



		$PHPExcel = new PHPExcel();

		$id_komoditas = $_GET['id_komoditas'];
		$id_lokasi = $_GET['id_lokasi'];
		$tanggal_awal = $_GET['tanggal_awal'];
		$tanggal_akhir = $_GET['tanggal_akhir'];		

		$PHPExcel->getActiveSheet()->getStyle('A3:M3')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle("A1:L1")->getFont()->setSize(14);
		$PHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
		$PHPExcel->getActiveSheet()->mergeCells('A1:M1');//sama jLga
		$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "DATA BAHAN");
		
		$PHPExcel->getActiveSheet()->setCellValue('A3', 'NO');
		$PHPExcel->getActiveSheet()->setCellValue('B3', 'TANGGAL');
		$PHPExcel->getActiveSheet()->setCellValue('C3', 'NAMA BAHAN');
		$PHPExcel->getActiveSheet()->setCellValue('D3', 'LOKASI');
		$PHPExcel->getActiveSheet()->setCellValue('E3', 'HARGA');

		$PHPExcel->getActiveSheet()->getStyle('A3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
		

		$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
		$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

		$i = 0;
		$kolom = 3;

		$tanggal = $tanggal_awal;
		$komoditas = Komoditas::model()->findByPk($id_komoditas);
		$lokasi = Lokasi::model()->findByPk($id_lokasi);

		while($tanggal<=$tanggal_akhir)
		{
			$i++; $kolom++;

			$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
			$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $tanggal);
			$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $komoditas->nama);
			$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $lokasi->nama);
			$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $komoditas->getHarga($id_lokasi,$tanggal));
			
			$tanggal = date('Y-m-d',strtotime($tanggal . "+1 days"));							
			
		}

		$PHPExcel->getActiveSheet()->getStyle('A4:D'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
		$PHPExcel->getActiveSheet()->getStyle('E4:E'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);//merge and center
		$PHPExcel->getActiveSheet()->getStyle('A3:E'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	
		$PHPExcel->getActiveSheet()->getStyle('A3:M'.$kolom)->getAlignment()->setWrapText(true);
		$PHPExcel->getActiveSheet()->getStyle('A3:M'.$kolom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
	
		$filename = time().'_HargaKomoditas.xlsx';

		$path = Yii::app()->basePath.'/../exports/';
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
		$objWriter->save($path.$filename);	
		$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);
	}	

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/

	public function loadModel($id)
	{
		$model=Komoditas::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='bahan-pokok-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
