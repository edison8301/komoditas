<?php

class InformasiController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update','naikUrutan','turunUrutan'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Informasi;

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);
	
		if(isset($_POST['Informasi']))
		{
			$model->attributes=$_POST['Informasi'];
			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s');	
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

	$this->render('create',array(
		'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Informasi']))
{
$model->attributes=$_POST['Informasi'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Informasi');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Informasi('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Informasi']))
$model->attributes=$_GET['Informasi'];

$this->render('admin',array(
'model'=>$model,
));
}

	public function actionNaikUrutan($id)
	{
		$model = $this->loadModel($id);
		if($model->urutan==1)
		{
			Yii::app()->user->setFlash('danger','Urutan data terkait sudah paling atas');
			$this->redirect(array('informasi/admin'));
		} else {
			$model_2 = Informasi::model()->findByAttributes(array('urutan'=>$model->urutan-1));
			
			$model->urutan--;
			$model->save();
			
			$model_2->urutan++;
			$model_2->save();
			
			$model->aturUrutan();
			
			Yii::app()->user->setFlash('success','Urutan data berhasil naik');
			$this->redirect(array('informasi/admin'));
		}
	}

	public function actionTurunUrutan($id)
	{
		$model = $this->loadModel($id);
		
		$jumlah = Informasi::model()->count();
		
		if($jumlah == $model->urutan)
		{
			Yii::app()->user->setFlash('danger','Urutan data terkait sudah terbawah');
			$this->redirect(array('informasi/admin'));
		} else {
		
			$model_2 = Informasi::model()->findByAttributes(array('urutan'=>$model->urutan+1));
			
			$model->urutan++;
			$model->save();
			$model_2->urutan--;
			$model_2->save();
			
			$model->aturUrutan();
			
			Yii::app()->user->setFlash('success','Urutan data berhasil turun');
			$this->redirect(array('informasi/admin'));
		}
		
		
	}	

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Informasi::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='informasi-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
