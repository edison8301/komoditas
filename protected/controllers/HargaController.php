<?php

class HargaController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index','view'),
			'users'=>array('*'),
		),
		array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('create','update','exportExcel'),
			'users'=>array('@'),
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
			'actions'=>array('admin','delete'),
			'users'=>array('admin'),
		),
		array('deny',  // deny all users
			'users'=>array('*'),
		),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Harga;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_GET['id_lokasi']))
			$model->id_lokasi = $_GET['id_lokasi'];
		
		if(isset($_GET['id_komoditas']))
			$model->id_komoditas = $_GET['id_komoditas'];
		
		$model->tanggal = date('Y-m-d');
		if(isset($_GET['tanggal']))
			$model->tanggal = $_GET['tanggal'];


		if(isset($_POST['Harga']))
		{
			$model->attributes=$_POST['Harga'];			
			
			date_default_timezone_get('Asia/Jakarta');
            $model->waktu_dibuat = date('Y-m-d H:i:s');

			$model->harga = str_replace('.','', $model->harga);
			$lama = Harga::model()->findByAttributes(array(
						'id_komoditas'=>$model->id_komoditas,
						'id_lokasi'=>$model->id_lokasi,
						'tanggal'=>$model->tanggal
			));
			
			if($lama!==null)
			{
				$lama->harga = $model->harga;
				$lama->save();
			} else {
				$model->save();
			}
			
			Yii::app()->user->setFlash('success', 'Data harga berhasil disimpan');

			if (Yii::app()->controller == "lokasi") {
				$this->redirect(array('lokasi/admin'));
			}

			$this->redirect(array('komoditas/index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model->harga = number_format($model->harga,0,',','.');
		if(isset($_POST['Harga']))
		{
			$model->attributes=$_POST['Harga'];
			$model->harga = str_replace('.','', $model->harga);
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success', 'Data harga berhasil disimpan');
				$this->redirect(array('komoditas/view','id'=>$model->id_komoditas));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Harga');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
	$model=new Harga('search');
	$model->unsetAttributes();  // clear any default values
	if(isset($_GET['Harga']))
	$model->attributes=$_GET['Harga'];

	$this->render('admin',array(
	'model'=>$model,
	));
}


	public function actionExportExcel($tanggal)
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPExcel',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));

		$PHPExcel = new PHPExcel();
			
		$PHPExcel->getActiveSheet()->setCellValue('A1', 'DAFTAR ISIAN HARGA RATA - RATA BEBERAPA BAHAN POKOK PANGAN ');
		$PHPExcel->getActiveSheet()->setCellValue('A2', 'DAN BARANG STRATEGIS LAINNYA DI KABUPATEN BELITUNG TIMUR');
		$PHPExcel->getActiveSheet()->setCellValue('A3', 'PROVINSI KEPULAUAN BANGKA BELITUNG');
		$PHPExcel->getActiveSheet()->setCellValue('A4', strtoupper('Hari: '.Helper::getHari($tanggal).', '.Helper::tanggal($tanggal)));
		$PHPExcel->getActiveSheet()->getStyle('A1:A4')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle('A1:A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$PHPExcel->getActiveSheet()->setCellValue('A8', 'NO');
		$PHPExcel->getActiveSheet()->setCellValue('B8', 'KOMODITI');
		$PHPExcel->getActiveSheet()->setCellValue('C8', 'SATUAN');
		$PHPExcel->getActiveSheet()->setCellValue('D8', 'LOKASI');
		$PHPExcel->getActiveSheet()->getStyle('A8:D8')->getFont()->setBold(true);

		$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
		$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);		
		$PHPExcel->getActiveSheet()->mergeCells('A8:A9');
		$PHPExcel->getActiveSheet()->mergeCells('B8:B9');
		$PHPExcel->getActiveSheet()->mergeCells('C8:C9');
		
		$ord = ord('C');
		
		foreach(Lokasi::model()->findAll() as $lokasi)
		{
			$ord++;
			
			$PHPExcel->getActiveSheet()->setCellValue(chr($ord).'9', strtoupper($lokasi->nama));
			$PHPExcel->getActiveSheet()->getStyle(chr($ord).'9')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getColumnDimension(chr($ord))->setWidth(35);
		}

		$PHPExcel->getActiveSheet()->mergeCells('D8:'.chr($ord).'8');

		$ord++;
		
		$PHPExcel->getActiveSheet()->setCellValue(chr($ord).'8', 'RATA');
		$PHPExcel->getActiveSheet()->getStyle(chr($ord).'8')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->mergeCells(chr($ord).'8:'.chr($ord).'9');

		$PHPExcel->getActiveSheet()->mergeCells('A1:'.chr($ord).'1');
		$PHPExcel->getActiveSheet()->mergeCells('A2:'.chr($ord).'2');
		$PHPExcel->getActiveSheet()->mergeCells('A3:'.chr($ord).'3');
		$PHPExcel->getActiveSheet()->mergeCells('A4:'.chr($ord).'4');
			
		$i = 0;
		$kolom = 10;

		foreach(Komoditas::model()->findAllByAttributes(array('id_induk'=>null),array('order'=>'urutan ASC')) as $data)
		{
			
			$i++; $kolom++;

			$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
			$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nama);

			if(!$data->hasSub()) 
			{			
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom,$data->satuan);
				$chr = ord('D');
				$dr = '';
				foreach(Lokasi::model()->findAll() as $lokasi)
				{
					$dr = $chr+1;
					$PHPExcel->getActiveSheet()->setCellValue(chr($chr).$kolom,$data->getHarga($lokasi->id,$tanggal));
					$PHPExcel->getActiveSheet()->setCellValue(chr($dr).$kolom,Harga::model()->getHargaRata($data->id,$tanggal));
					$chr++;
				}
			}

			if($data->hasSub()) 
			{
				foreach($data->getSub() as $subdata)
				{
					$kolom++;
					$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, '---'.$subdata->nama);
					$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $subdata->satuan);

					$chr = ord('D');
					$dr = '';
					foreach(Lokasi::model()->findAll() as $lokasi)
					{
						$dr = $chr+1;
						$PHPExcel->getActiveSheet()->setCellValue(chr($chr).$kolom, $subdata->getHarga($lokasi->id,$tanggal));
						$PHPExcel->getActiveSheet()->setCellValue(chr($dr).$kolom,$subdata->getHargaRata($data->id,$tanggal));
						$chr++;
					}
				}
			} 	
		}	
		
		$PHPExcel->getActiveSheet()->getStyle('A8'.$kolom.':D'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		
		$PHPExcel->getActiveSheet()->getStyle('A8:F9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	  	$PHPExcel->getActiveSheet()->getStyle('A11:A'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	  	$PHPExcel->getActiveSheet()->getStyle('C11:C'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$filename = time().'_HargaKomoditas.xlsx';

		$path = Yii::app()->basePath.'/../exports/';
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
		$objWriter->save($path.$filename);	
		$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);

	}



/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Harga::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='harga-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
