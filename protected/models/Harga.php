<?php

/**
 * This is the model class for table "Harga".
 *
 * The followings are the available columns in table 'Harga':
 * @property integer $id
 * @property integer $id_bahan_pokok
 * @property integer $id_lokasi
 * @property string $harga
 * @property string $tanggal_input
 */
class Harga extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'harga';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_komoditas, tanggal, id_lokasi, harga', 'required'),
			array('id_komoditas, id_lokasi ', 'numerical', 'integerOnly'=>true),
			array('harga', 'length', 'max'=>20),		
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_stok, id_komoditas, id_lokasi, harga, tanggal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_komoditas' => 'Komoditas',
			'id_lokasi' => 'Lokasi',			
			'harga' => 'Harga',
			'tanggal' => 'Tanggal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_komoditas',$this->id_komoditas);
		$criteria->compare('id_lokasi',$this->id_lokasi);		
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('tanggal',$this->tanggal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function cari($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		if(isset($_GET['Harga']))
			$this->attributes = $_GET['Harga'];

		$criteria=new CDbCriteria;


		$criteria->compare('id',$this->id);
		$criteria->compare('id_komoditas',$id);
		$criteria->compare('id_lokasi',$this->id_lokasi);		
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('tanggal',$this->tanggal,true);

		$criteria->order = 'tanggal DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}		

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Harga the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getHarga($id_komoditas,$id_lokasi,$tanggal)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'id_komoditas=:id_komoditas AND id_lokasi=:id_lokasi AND tanggal <= :tanggal';
		$criteria->params = array(
			':id_komoditas'=>$id_komoditas,
			':id_lokasi'=>$id_lokasi,
			':tanggal'=>$tanggal
		);
		$criteria->order='tanggal DESC';
			
		$model = Harga::model()->find($criteria);

		if($model!==null)
			return $model->harga;
		else
			return null;
	}
	
	public function getHargaRata($id_komoditas,$tanggal)
	{
		$komoditas = Komoditas::model()->findByPk($id_komoditas);
		
		if($komoditas->hasSub())
			return null;

		$hargaRata = 0;
		
		$i=0;
		
		$model = Lokasi::model()->findAll();
		
		$harga = 0;
		foreach($model as $lokasi)
		{
			$harga = $harga + $this->getHarga($id_komoditas,$lokasi->id,$tanggal);
			$i++;
		}
		
		if($i!=0)
			$hargaRata = $harga/$i;
		else
			$hargaRata = 0;
			
		return $hargaRata;
	}

	public static function getGrafik($id_lokasi,$id_komoditas)
	{
		$grafik = '';
		
		$bahan = Komoditas::model()->findByPk($id_komoditas);

		$tanggal_sekarang = date('Y-m-d');
		$tanggal = date( 'Y-m-d',strtotime("-30 days"));

		while($tanggal<=$tanggal_sekarang)
		{
			$grafik .= '{"label":"'.Helper::getTanggalChart($tanggal).'","value":"'.$bahan->getHarga($id_lokasi,$tanggal).'"},';		
			$tanggal = date('Y-m-d',strtotime($tanggal . "+1 days"));							
			
		} 

		return $grafik;
	}

	public function getPersentaseHarga($id_komoditas,$id_lokasi,$tanggal)
	{

		$komoditas = Komoditas::model()->findByPk($id_komoditas);
		$lokasi = Lokasi::model()->findByPk($id_lokasi);

		if($komoditas->hasSub())
			return null;

		$hariIni = $tanggal;
    	$kemarin = date( 'Y-m-d',strtotime('-1 days',strtotime($tanggal)));
			
		$hargaHariIni = Harga::model()->getHarga($id_komoditas,$id_lokasi,$hariIni);
		$hargaKemarin = Harga::model()->getHarga($id_komoditas,$id_lokasi,$kemarin);


		if ($hargaKemarin != null) {
			return round(($hargaHariIni-$hargaKemarin)/$hargaKemarin * 100,1);
		} else return null;

	}

	


	public function getNamaKomoditas()
	{
		$model = Komoditas::model()->findByPk($this->id_komoditas);
		if($model!==null) {
			return $model->nama;
		} else {
			return null;
		}
/*		$model = Komoditas::model()->findByPk($this->id_komoditas);

		if (Komoditas::model()->countByAttributes(array('id_induk'=>$this->id)) > 0) {
			return $model->nama;
		} else {
			return null;
		}*/
	}

	public function getNamaSatuan()
	{
		$model = Komoditas::model()->findByPk($this->id_komoditas);
		if($model !==null)
			return $model->satuan;
		else
			return null;
	}

	public function getNamaLokasi()
	{
		$model = Lokasi::model()->findByPk($this->id_lokasi);
		if($model !==null)
			return $model->nama;
		else
			return null;
	}	

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}	

	

	
}
