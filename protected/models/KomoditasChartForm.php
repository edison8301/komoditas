<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class KomoditasChartForm extends CFormModel
{
	public $komoditas;
	public $lokasi;

	public function rules()
	{
		return array(
			array('komoditas, lokasi', 'required'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'komoditas'=>'Komoditas',
			'lokasi'=>'Lokasi',
			'tanggal_awal'=>'Tanggal Awal',
			'tanggal_akhir'=>'Tanggal Akhir'
		);
	}
}
