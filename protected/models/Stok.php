<?php

/**
 * This is the model class for table "stok".
 *
 * The followings are the available columns in table 'stok':
 * @property integer $id
 * @property integer $id_komoditas
 * @property integer $id_lokasi
 * @property string $stok
 * @property string $tanggal
 * @property string $waktu_dibuat
 */
class Stok extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stok';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_komoditas, id_lokasi, stok', 'required'),
			array('id_komoditas, id_lokasi', 'numerical', 'integerOnly'=>true),
			array('stok', 'length', 'max'=>20),
			array('tanggal, waktu_dibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_komoditas, id_lokasi, stok, tanggal, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_komoditas' => 'Komoditas',
			'id_lokasi' => 'Lokasi',
			'stok' => 'Stok',
			'tanggal' => 'Tanggal',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_komoditas',$this->id_komoditas);
		$criteria->compare('id_lokasi',$this->id_lokasi);
		$criteria->compare('stok',$this->stok,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function cari($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		if(isset($_GET['Stok']))
			$this->attributes = $_GET['Stok'];

		$criteria=new CDbCriteria;


		$criteria->compare('id',$this->id);
		$criteria->compare('id_komoditas',$id);
		$criteria->compare('id_lokasi',$this->id_lokasi);		
		$criteria->compare('stok',$this->stok,true);
		$criteria->compare('tanggal',$this->tanggal,true);

		$criteria->order = 'tanggal DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stok the static model class
	 */
	
	public static function getStok($id_komoditas,$id_lokasi,$tanggal)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'id_komoditas=:id_komoditas AND id_lokasi=:id_lokasi AND tanggal <= :tanggal';
		$criteria->params = array(
			':id_komoditas'=>$id_komoditas,
			':id_lokasi'=>$id_lokasi,
			':tanggal'=>$tanggal
		);
		$criteria->order='tanggal DESC';
			
		$model = Stok::model()->find($criteria);

		if($model!==null)
			return $model->stok;
		else
			return null;
	}

	public function getPersentaseStok($id_komoditas,$id_lokasi,$tanggal)
	{

		$komoditas = Komoditas::model()->findByPk($id_komoditas);
		$lokasi = Lokasi::model()->findByPk($id_lokasi);

		if($komoditas->hasSub())
			return null;

		$hariIni = $tanggal;
    	$kemarin = date( 'Y-m-d',strtotime('-1 days',strtotime($tanggal)));
			
		$stokHariIni = Stok::model()->getStok($id_komoditas,$id_lokasi,$hariIni);
		$stokKemarin = Stok::model()->getStok($id_komoditas,$id_lokasi,$kemarin);


		if ($stokKemarin != null) {
			return round(($stokHariIni-$stokKemarin)/$stokKemarin * 100,1);
		} else return null;

	}

	public function getNamaKomoditas()
	{
		$model = Komoditas::model()->findByPk($this->id_komoditas);
		if($model!==null) {
			return $model->nama;
		} else {
			return null;
		}
/*		$model = Komoditas::model()->findByPk($this->id_komoditas);

		if (Komoditas::model()->countByAttributes(array('id_induk'=>$this->id)) > 0) {
			return $model->nama;
		} else {
			return null;
		}*/
	}

	public function getNamaLokasi()
	{
		$model = Lokasi::model()->findByPk($this->id_lokasi);
		if($model !==null)
			return $model->nama;
		else
			return null;
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



}
