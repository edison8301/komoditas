<?php

/**
 * This is the model class for table "gambar".
 *
 * The followings are the available columns in table 'gambar':
 * @property integer $id
 * @property string $nama
 * @property string $gambar
 * @property integer $id_kategori
 */
class Gambar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gambar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, gambar, id_kategori', 'required'),
			array('id_kategori', 'numerical', 'integerOnly'=>true),
			array('nama, gambar', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, gambar, id_kategori', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			"kategori"=>array(self::BELONGS_TO,'GambarKategori','id_kategori'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'gambar' => 'Gambar',
			'id_kategori' => 'Kategori',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id_kategori)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('gambar',$this->gambar,true);
		$criteria->compare('id_kategori',$id_kategori);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gambar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function getGambar($htmlOptions=array())
	{
		if($this->gambar !='') {
			return CHtml::image(Yii::app()->baseUrl.'/uploads/gambar/'.$this->gambar,'',$htmlOptions);
		} else {			
			return CHtml::image(Yii::app()->baseUrl.'/images/no-image.png','',$htmlOptions);
		}
	}

	public function getGambarById($id)
	{
		$model = self::model()->findByPk(['id'=>$id]);
		return $model;
	}


	public function getGambarCarousel($limit=null)
	{
		$carousel = array();
		foreach(Gambar::model()->findAll(array('order'=>'id ASC')) as $data)
		{			
			$carousel[] = array('image'=>(Yii::app()->baseUrl.'/uploads/gambar/'.$data->gambar),'caption'=>CHtml::link($data->nama),array('width'=>'30'));
		}
		return $carousel;
	}
	


}
