<?php

/**
 * This is the model class for table "bahan_pokok".
 *
 * The followings are the available columns in table 'bahan_pokok':
 * @property integer $id
 * @property integer $id_parent
 * @property integer $urutan
 * @property string $nama_bahan
 * @property string $satuan
 */
class Komoditas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'komoditas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, nama', 'required'),
			array('id_induk, urutan, id_status', 'numerical', 'integerOnly'=>true),
			array('nama, satuan', 'length', 'max'=>255),
			array('satuan, id_induk, urutan, id_status','safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_induk, id_status, urutan, nama, satuan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'status'=>array(self::BELONGS_TO,'KomoditasStatus','id_status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_induk' => 'Komoditas Induk',
			'urutan' => 'Urutan',
			'nama' => 'Nama Bahan',
			'satuan' => 'Satuan',
			'id_status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_induk',$this->id_induk);
		$criteria->compare('urutan',$this->urutan);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('id_status',$this->id_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BahanPokok the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getSub() {
		return Komoditas::model()->findAllByAttributes(array('id_induk'=>$this->id),array('order'=>'urutan ASC'));	
	}

	public function findAllSub() {
		return Komoditas::model()->findAllByAttributes(array('id_induk'=>$this->id),array('order'=>'urutan ASC'));	
	}
	
	public function hasSub() {
		if (Komoditas::model()->countByAttributes(array('id_induk'=>$this->id)) > 0)
			return true;
		else
			return false;
	}

	
	public function setUrutan()
	{
		if(empty($this->id_induk));
			$id_induk = null;
		
		$total = Komoditas::model()->countByAttributes(array('id_induk'=>$id_induk));
		$total++;
		$this->urutan = $total;
	}
	
	public function aturUrutan($id_induk=null)
	{
		if($id_induk==null)
			$id_induk = $this->id_induk;
		
		
		$model = Komoditas::model()->findAllByAttributes(array('id_induk'=>$id_induk),array('order'=>'urutan ASC'));
		
		$i=1;
		foreach($model as $data)
		{
			$data->urutan = $i;
			$data->save();
			$i++;
		}
	}

	public function getBahan()
	{
		$model = Harga::model()->findByAttributes(array('id' => $this->id_komoditas));

		if($model !==null)
			return $model->nama;
		else
			return null;
	}

	public function getParent()
	{
		$model = Komoditas::model()->findByPk($this->id_induk);

		if($model !==null)
			return $model->nama;
		else
			return null;
	}	

	public function getLokasi()
	{
		$model = Harga::model()->findByAttributes(array('id' => $this->id_lokasi));

		if($model !==null)
			return $model->nama;
		else
			return null;
	}

	public function getHarga($id_lokasi,$tanggal)
	{
		$criteria = new CDbCriteria;
		$params = array();

		if($tanggal>date('Y-m-d'))
			return "N/A";

		$criteria->addCondition('id_komoditas = :id_komoditas');
		$params[':id_komoditas']=$this->id;

		$criteria->addCondition('id_lokasi = :id_lokasi');
		$params[':id_lokasi']=$id_lokasi;

		$criteria->addCondition('tanggal <= :tanggal');
		$params[':tanggal']=$tanggal;

		$criteria->params = $params;
		$criteria->order = 'tanggal DESC';

		$model = Harga::model()->find($criteria);

		if($model!==null)
			return $model->harga;
		else
			return 0;
	}

	public function getHargaRata($id_komoditas,$tanggal)
	{
		
		$hargaRata = 0;
		
		$i=0;
		
		$model = Lokasi::model()->findAll();
		
		$harga = 0;
		foreach($model as $lokasi)
		{
			$harga = $harga + $this->getHarga($lokasi->id,$tanggal);
			$i++;
		}
		
		if($i!=0)
			$hargaRata = $harga/$i;
		else
			$hargaRata = 0;
			
		return $hargaRata;
	}

	public static function getListKomoditas()
	{
		$list = array();
		
		$criteria = new CDbCriteria;
		$criteria->order = 'urutan ASC';

		foreach(Komoditas::model()->findAllByAttributes(array('id_induk'=>null),$criteria) as $data) {
			$list[$data->id]=$data->nama;
			if($data->hasSub()) {
				foreach($data->getSub() as $sub)
				{
					$list[$sub->id]='--- '.$sub->nama;
				}
			}
		}
		return $list;		
	}

	public static function getNamaKomoditasStatis($id)
	{
		$model = Komoditas::model()->findByPk($id);
		if($model !==null)
			return $model->nama;
		else
			return null;
	}

	public static function findAllKomoditasInduk()
	{
		return Komoditas::model()->findAllByAttributes(array('id_induk'=>null),array('order'=>'urutan ASC'));
	}

	public function getSatuan()
	{
		if($this->hasSub())
		{
			return null;
		} else {
			return $this->satuan;
		}
	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}	

	public static function getStatus()
	{
		return CHtml::listData(KomoditasStatus::model()->findAll(),'id','nama');
	}

	public static function findAllKomoditas()
	{
		return self::model()->findAllByAttributes(array('id_induk'=>null),array('order'=>'urutan ASC'));
	}


}
