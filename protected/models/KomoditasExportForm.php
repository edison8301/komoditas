<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class KomoditasExportForm extends CFormModel
{
	public $id_komoditas;
	public $id_lokasi;
	public $tanggal_awal;
	public $tanggal_akhir;

	public function rules()
	{
		return array(
			array('id_komoditas, id_lokasi, tanggal_awal, tanggal_akhir', 'required'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_komoditas'=>'komoditas',
			'id_lokasi'=>'Lokasi'
		);
	}
}
