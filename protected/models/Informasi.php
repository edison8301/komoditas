<?php

/**
 * This is the model class for table "informasi".
 *
 * The followings are the available columns in table 'informasi':
 * @property integer $id
 * @property string $nama_singkat
 * @property string $nama_informasi
 * @property integer $urutan
 * @property string $waktu_dibuat
 */
class Informasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'informasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_informasi', 'required'),
			array('urutan', 'numerical', 'integerOnly'=>true),
			array('nama_singkat', 'length', 'max'=>255),
			array('waktu_dibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_singkat, nama_informasi, urutan, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_singkat' => 'Nama Singkat',
			'nama_informasi' => 'Nama Informasi',
			'urutan' => 'Urutan',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_singkat',$this->nama_singkat,true);
		$criteria->compare('nama_informasi',$this->nama_informasi,true);
		$criteria->compare('urutan',$this->urutan);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);
		$criteria->order = 'urutan ASC';
 
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Informasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function aturUrutan()
	{
		
		$model = Informasi::model()->findAll(array('order'=>'urutan ASC'));
		
		$i=1;
		foreach($model as $data)
		{
			$data->urutan = $i;
			$data->save();
			$i++;
		}
	}
}
